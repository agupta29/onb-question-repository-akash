﻿(STARTIGNORE)

Note: Any text within the (STARTIGNORE) and (ENDIGNORE) tags will be ignored when importing this question template

INSTRUCTIONS
Fill in the blank question templates below then upload and check them under the "Tests -> Question bank -> Import questions" section.
Maximum of 50 questions can be uploaded at once

Your categories numbers are listed below for your reference to add to the (Category): line
For example, the line below will use the 'Generic' category

(Category):

Your categories:
0 Generic
1 Java
2 Axure
3 JIRA
4 OOP
5 SDLC
6 Communication
7 Unix Scripting
8 HTML
9 CSS
10 JavaScript
11 Angular JS
12 Design Patterns
13 Git
14 Java Design Pattern
15 Business Analysis
16 Version Control
17 Presentations
18 Software Testing
19 Foundations of Programming: Software Quality Assurance
20 PowerPoint 2013 Essential Training
21 SQL Essential Training
22 Web Design Fundamentals

(Grade style): Only applicable for 'multiplechoice' (with more then 1 correct answer) style questions
0: Give full score only when all correct responses are provided
1: Give partial score per correct response and deduct partial score for incorrect responses
2: Give partial score per correct response but do not deduct partial score for incorrect responses

(Submitter): This is the name of the person creating and submitting the questions.

(Course Site): This is the site the course was taken on. The options for this are Lynda, and Udemy.

(Course Name): This is the exact name of the course as it is written on the site.

(Course URL): The URL of the course.

(Discipline): This is what discipline the training is classified under, Professional or Technical.

(Hint): This is an optional field that can be used if you wish to provide a hint to the user about the question.

(Subject): This is similar to category but can be used to specify more specific detail on the category.

(Difficulty): This is a range of difficulty of the question, Beginner, Intermediate, and Advanced.

(Applicability): This is if the question is about the course or can be applied to the category in general. Course or General are accepted choices for this section.


(ENDIGNORE)

(STARTIGNORE)
Quiz Details
***Category may need to be changed/updated
(Submitter): Joseph Duda
(Course Site): Udemy
(Course Name): ISTQB Certified Tester Foundation Level (CTFL) Training
(Course URL): https://www.udemy.com/istqb-certified-tester-foundation-level-training/?dtcode=VZpyHCj1CtgS
(Discipline): Professional
(ENDIGNORE)


(Type): truefalse
(Category): 18
(Grade style): 0
(Random answers): 0
(Question): It is cheaper to fix bugs after software development is complete because then the developers will have a lot more free-time.
(A): True
(B): False
(Correct): B
(Points): 1
(CF): Test early and test often. The earlier a defect is discovered, the less resources it will require to fix.
(WF): Test early and test often. The earlier a defect is discovered, the less resources it will require to fix.
(STARTIGNORE)
(Hint):
(Subject): ISTQB
(Difficulty): Beginner
(Applicability): course
(ENDIGNORE)


(Type): multiplechoice
(Category): 18
(Grade style): 0
(Random answers): 1
(Question): The three main objectives of software testing are:
(A): Determine that the software meets the product requirements as specified.
(B): Demonstrate the software is fit-for-use.
(C): Save the organization money.
(D): Find defects and bugs.
(E): To become familiar with how end-users will use the software.
(Correct): A,B,D
(Points): 1
(CF): A, B, and D are the three main objectives. C should be a side-effect of meeting the main objectives. E is not a testing objective.
(WF): A, B, and D are the three main objectives. C should be a side-effect of meeting the main objectives. E is not a testing objective.
(STARTIGNORE)
(Hint):
(Subject): ISTQB
(Difficulty): Beginner
(Applicability): course
(ENDIGNORE)


(Type): truefalse
(Category): 18
(Grade style): 0
(Random answers): 0
(Question): In most situations, exhaustive testing is impossible.
(A): True
(B): False
(Correct): A
(Points): 1
(CF): The sheer number of possible valid and invalid inputs for even the most basic applications can become astronomical very quickly.
(WF): The sheer number of possible valid and invalid inputs for even the most basic applications can become astronomical very quickly.
(STARTIGNORE)
(Hint):
(Subject): ISTQB
(Difficulty): Beginner
(Applicability): course
(ENDIGNORE)


(Type): truefalse
(Category): 18
(Grade style): 0
(Random answers): 0
(Question): Thorough testing can prove there are no more defects in a product.
(A): True
(B): False
(Correct): B
(Points): 1
(CF): Testing can reduce the probability of undiscovered defects, but can never guarantee there are no more defects in a product.
(WF): Testing can reduce the probability of undiscovered defects, but can never guarantee there are no more defects in a product.
(STARTIGNORE)
(Hint):
(Subject): ISTQB
(Difficulty): Beginner
(Applicability): course
(ENDIGNORE)


(Type): multiplechoice
(Category): 18
(Grade style): 0
(Random answers): 1
(Question): Defect clustering refers to which of the following?
(A): When one bug is fixed, it often causes two or more new bugs.
(B): Most defects are found during the alpha stage of a product.
(C): Many defects are found in a particularly complex section of code.
(D): A specific test case that never manages to pass.
(E): The test team always seems to find a lot of defects on Tuesdays.
(Correct): C
(Points): 1
(CF): Defect clustering refers to the fact that oftentimes many defects are found in a particularly complex section of code.
(WF): Defect clustering refers to the fact that oftentimes many defects are found in a particularly complex section of code.
(STARTIGNORE)
(Hint):
(Subject): ISTQB
(Difficulty): Beginner
(Applicability): course
(ENDIGNORE)


(Type): multiplechoice
(Category): 18
(Grade style): 0
(Random answers): 1
(Question): Which of the following is not a stage in the fundamental test process?
(A): Product requirements meeting.
(B): Test analysis and design.
(C): Test implementation and execution.
(D): Evaluate exit criteria and reporting.
(E): Test closure activities.
(Correct): A
(Points): 1
(CF): The first step should be Planning and Control, which may include discussions about the product requirements. The rest of the steps mentioned are in the correct order.
(WF): The first step should be Planning and Control, which may include discussions about the product requirements. The rest of the steps mentioned are in the correct order.
(STARTIGNORE)
(Hint):
(Subject): ISTQB
(Difficulty): Beginner
(Applicability): course
(ENDIGNORE)


(Type): multiplechoice
(Category): 18
(Grade style): 0
(Random answers): 1
(Question): When you use tools to examine software code without running it, you are doing what?
(A): Dynamic analysis.
(B): Static analysis.
(C): Functional Testing.
(D): Regressions testing.
(E): Unit testing.
(Correct): B
(Points): 1
(CF): Static analysis examines code without running it in the hopes of eliminating as many defects as possible before ever running the program.
(WF): Static analysis examines code without running it in the hopes of eliminating as many defects as possible before ever running the program.
(STARTIGNORE)
(Hint):
(Subject): ISTQB
(Difficulty): Beginner
(Applicability): course
(ENDIGNORE)


(Type): multiplechoice
(Category): 18
(Grade style): 0
(Random answers): 1
(Question): A black-box technique to greatly reduce the number of tests run by grouping test cases into sets of similar items is known as?
(A): Decision table testing.
(B): Boundary value analysis.
(C): State transition testing.
(D): Use case testing.
(E): Equivalence partitioning.
(Correct): E
(Points): 1
(CF): Equivalence partitioning is what the question refers to. The others are alternative black-box techniques.
(WF): Equivalence partitioning is what the question refers to. The others are alternative black-box techniques.
(STARTIGNORE)
(Hint):
(Subject): ISTQB
(Difficulty): Intermediate
(Applicability): course
(ENDIGNORE)


(Type): multiplechoice
(Category): 18
(Grade style): 0
(Random answers): 1
(Question): Test coverage refers to which of the following?
(A): How much of the developers' testing and the testers' testing overlap.
(B): The percent of testing that is complete.
(C): How many of the cases your tests cover compared to the total number of possible cases.
(D): Defects that hide under the covers for an extended period of time.
(E): None of the above.
(Correct): C
(Points): 1
(CF): Test coverage is the degree to which a specified coverage item has been exercised by the test suite. It is a percent calculated as: 100 * (# of items tested) / (total # of items)
(WF): Test coverage is the degree to which a specified coverage item has been exercised by the test suite. It is a percent calculated as: 100 * (# of items tested) / (total # of items)
(STARTIGNORE)
(Hint):
(Subject): ISTQB
(Difficulty): Intermediate
(Applicability): course
(ENDIGNORE)


(Type): multiplechoice
(Category): 18
(Grade style): 0
(Random answers): 1
(Question): Your manager wants to know how well an application will work if the database becomes extremely large. Which of the following reports should you supply them with?
(A): Environment test.
(B): Load test.
(C): Dependency test.
(D): Volume test.
(E): Configuration test.
(Correct): D
(Points): 1
(CF): A volume test is specifically designed for testing the product when there is a large volume of data involved.
(WF): A volume test is specifically designed for testing the product when there is a large volume of data involved.
(STARTIGNORE)
(Hint):
(Subject): ISTQB
(Difficulty): Advanced
(Applicability): course
(ENDIGNORE)


(Type): multiplechoice
(Category): 18
(Grade style): 0
(Random answers): 1
(Question): What is a good reason to use test tools?
(A): Testing is time consuming and tedious.
(B): Many tests need to be repeated for each build.
(C): Tools can help with test and defect management.
(D): Tools can help to measure software quality.
(E): All of the above.
(Correct): E
(Points): 1
(CF): All of the answers are good reasons to use test tools.
(WF): All of the answers are good reasons to use test tools.
(STARTIGNORE)
(Hint):
(Subject): ISTQB
(Difficulty): Beginner
(Applicability): course
(ENDIGNORE)


(Type): multiplechoice
(Category): 18
(Grade style): 0
(Random answers): 1
(Question): Which of the following is the lowest level of independence?
(A): Tests are designed by the person who wrote the code.
(B): Tests are designed by a developer other than the one who wrote the code.
(C): Tests are designed by an independent test team.
(D): Tests are designed by an outsourced test team.
(Correct): A
(Points): 1
(CF): The more separate the test design is from the code development, the higher the level of independence. The lowest level of independence is when the code writer is designing the tests.
(WF): The more separate the test design is from the code development, the higher the level of independence. The lowest level of independence is when the code writer is designing the tests.
(STARTIGNORE)
(Hint):
(Subject): ISTQB - Sample Test
(Difficulty): Intermediate
(Applicability): course
(ENDIGNORE)


(Type): multiplechoice
(Category): 18
(Grade style): 0
(Random answers): 1
(Question): Which of the following is a correct statement?
(A): A developer makes a mistake which causes a defect that may be seen as a failure during dynamic testing.
(B): A developer makes an error which results in a failure that may be seen as a fault when the software is executed.
(C): A developer has a failure which results in a defect that may be seen as a mistake during dynamic testing.
(D): A developer makes a mistake which causes a bug that may be seen as a defect when the software is executed.
(Correct): A
(Points): 1
(CF): A is correct. The developer makes a mistake/error which causes a defect/fault/bug which may cause a failure when the code is dynamically tested or executed. B is incorrect because fault and failure are reversed. C is incorrect because failure and mistake are reversed. D is incorrect because it’s a failure that’s seen during execution, not the defect itself. The failure is a symptom of the defect.
(WF): A is correct. The developer makes a mistake/error which causes a defect/fault/bug which may cause a failure when the code is dynamically tested or executed. B is incorrect because fault and failure are reversed. C is incorrect because failure and mistake are reversed. D is incorrect because it’s a failure that’s seen during execution, not the defect itself. The failure is a symptom of the defect.
(STARTIGNORE)
(Hint):
(Subject): ISTQB - Sample Test
(Difficulty): Advanced
(Applicability): course
(ENDIGNORE)


(Type): multiplechoice
(Category): 18
(Grade style): 0
(Random answers): 1
(Question): Which of the following is the correct list of the triggers for maintenance testing?
(A): A component in production is modified, migrated or retired.
(B): A fix has been received for a product that is in development.
(C): While running in the test environment, a regression has been discovered in a set of fixes just received from the developer.
(D): A new requirement has been received for the software that is currently under test that may result in an architectural change.
(Correct): A
(Points): 1
(CF): Maintenance testing is conducted for products that have been released to production that have been modified, migrated or had some component retired. B is not correct because the product is still in development, not in production. C is not correct because the product is still in testing and has not been released yet. D is not correct because this is a product that is still in development that has not yet been released to production.
(WF): Maintenance testing is conducted for products that have been released to production that have been modified, migrated or had some component retired. B is not correct because the product is still in development, not in production. C is not correct because the product is still in testing and has not been released yet. D is not correct because this is a product that is still in development that has not yet been released to production.
(STARTIGNORE)
(Hint):
(Subject): ISTQB - Sample Test
(Difficulty): Advanced
(Applicability): course
(ENDIGNORE)


(Type): multiplechoice
(Category): 18
(Grade style): 0
(Random answers): 1
(Question): Which of the following is a type of issue that is best found in static analysis?
(A): An inaccurate formula.
(B): A memory leak.
(C): A piece of dead code.
(D): A problem with the code not matching the requirements.
(Correct): C
(Points): 1
(CF): Dead code should be detected by a static analysis tool and it can be quite hard to find any other way. A is not correct because this is better found by a code review or dynamic testing. B is better found by dynamic testing. D is best found in a code review where humans are checking the code against the requirements. This could also be caught during system testing.
(WF): Dead code should be detected by a static analysis tool and it can be quite hard to find any other way. A is not correct because this is better found by a code review or dynamic testing. B is better found by dynamic testing. D is best found in a code review where humans are checking the code against the requirements. This could also be caught during system testing.
(STARTIGNORE)
(Hint):
(Subject): ISTQB - Sample Test
(Difficulty): Intermediate
(Applicability): course
(ENDIGNORE)


(Type): multiplechoice
(Category): 18
(Grade style): 0
(Random answers): 1
(Question): If test cases are derived from looking at the code, what type of test design technique is being used?
(A): Black-box.
(B): White-box.
(C): Specification-based.
(D): Behavior-based.
(Correct): B
(Points): 1
(CF): White-box is correct. The other choices are all black-box and use the specifications or requirements for the test design.
(WF): White-box is correct. The other choices are all black-box and use the specifications or requirements for the test design.
(STARTIGNORE)
(Hint):
(Subject): ISTQB - Sample Test
(Difficulty): Intermediate
(Applicability): course
(ENDIGNORE)


(Type): multiplechoice
(Category): 18
(Grade style): 0
(Random answers): 1
(Question): If you are using error guessing to target your testing, which type of testing are you doing?
(A): Specification-based.
(B): Structure-based.
(C): Experience-based.
(D): Reference-based.
(Correct): C
(Points): 1
(CF): Error guessing is an experience-based technique. 
(WF): Error guessing is an experience-based technique. 
(STARTIGNORE)
(Hint):
(Subject): ISTQB - Sample Test
(Difficulty): Intermediate
(Applicability): course
(ENDIGNORE)


(Type): multiplechoice
(Category): 18
(Grade style): 0
(Random answers): 1
(Question): Shown below is the conversion key for a machine that scores exam papers and assigns grades. If you apply equivalence partitioning, how many test cases will you need to achieve minimum test coverage? <br>  1-49 = F, 50-59 = D-, 60-69 = D, 70-79 = C, 80-89 = B, 90-100=A
(A): 6
(B): 8
(C): 10
(D): 12
(Correct): B
(Points): 1
(CF): There should be 8 test cases: a test for the invalid too low (0 or less), one for each valid partition (6 of them), and one for invalid too high (>100).
(WF): There should be 8 test cases: a test for the invalid too low (0 or less), one for each valid partition (6 of them), and one for invalid too high (>100).
(STARTIGNORE)
(Hint):
(Subject): ISTQB - Sample Test
(Difficulty): Intermediate
(Applicability): course
(ENDIGNORE)


(Type): multiplechoice
(Category): 18
(Grade style): 0
(Random answers): 1
(Question): Shown below is the conversion key for a machine that scores exam papers and assigns grades. If you apply boundary value analysis, how many test cases will you need to achieve minimum test coverage? <br>  1-49 = F, 50-59 = D-, 60-69 = D, 70-79 = C, 80-89 = B, 90-100=A
(A): 8
(B): 10
(C): 12
(D): 14
(Correct): D
(Points): 1
(CF): You need 14 test cases: 0, 1, 49, 50, 59, 60, 69, 70, 79, 80, 89, 90, 100, 101
(WF): You need 14 test cases: 0, 1, 49, 50, 59, 60, 69, 70, 79, 80, 89, 90, 100, 101
(STARTIGNORE)
(Hint):
(Subject): ISTQB - Sample Test
(Difficulty): Intermediate
(Applicability): course
(ENDIGNORE)


(Type): multiplechoice
(Category): 18
(Grade style): 0
(Random answers): 1
(Question): Given the following pseudocode, what is the minimum number of test cases needed to achieve 100% statement coverage? <br>if day = Monday then statement a<br>else statement b<br>end if<br>if day = Tuesday then statement c<br>end if
(A): 1
(B): 2
(C): 3
(D): 4
(Correct): B
(Points): 1
(CF): Only two tests are needed, one with day = Monday and one with day = Tuesday. The Tuesday test will test the first else statement and the second if statement.
(WF): Only two tests are needed, one with day = Monday and one with day = Tuesday. The Tuesday test will test the first else statement and the second if statement.
(STARTIGNORE)
(Hint):
(Subject): ISTQB - Sample Test
(Difficulty): Intermediate
(Applicability): course
(ENDIGNORE)


(Type): multiplechoice
(Category): 18
(Grade style): 0
(Random answers): 1
(Question): You have been given the following set of six test cases to run. You have been instructed to run them in order by risk and to accomplish the testing as quickly as possible to provide feedback to the developers as soon as possible. Some tests cannot be run until after other tests. Given this information, what is the best order in which to run these tests? <br>Test Case ID - Duration - Risk Priority - Dependency <br>1 - 30 mins - Low - 6 <br>2 - 10 mins - Medium - none <br>3 - 45 mins - High - 1 <br>4 - 30 mins - High - 2 <br>5 - 10 mins - Medium - 4 <br>6 - 15 mins - Low - 2
(A): 2, 4, 5, 6, 1, 3
(B): 4, 3, 2, 5, 6, 1
(C): 2, 5, 6, 4, 1, 3
(D): 6, 1, 3, 2, 4, 5
(Correct): A
(Points): 1
(CF): A addresses the highest risk and fastest tests first. It runs a fast medium test before a slow and more dependent high risk test because this will give feedback to the developers more quickly.
(WF): A addresses the highest risk and fastest tests first. It runs a fast medium test before a slow and more dependent high risk test because this will give feedback to the developers more quickly.
(STARTIGNORE)
(Hint):
(Subject): ISTQB - Sample Test
(Difficulty): Advanced
(Applicability): course
(ENDIGNORE)


(Type): multiplechoice
(Category): 18
(Grade style): 0
(Random answers): 1
(Question): Which document specifies the inputs and outputs for a test?
(A): Test design specification.
(B): Test case specification.
(C): Test procedure specification.
(D): Test plan.
(Correct): B
(Points): 1
(CF): The inputs and outputs are defined in the test case specification.
(WF): The inputs and outputs are defined in the test case specification.
(STARTIGNORE)
(Hint):
(Subject): ISTQB - Sample Test
(Difficulty): Intermediate
(Applicability): course
(ENDIGNORE)


(Type): multiplechoice
(Category): 18
(Grade style): 0
(Random answers): 1
(Question): Usability testing is an example of which type of testing?
(A): Functional.
(B): Non-functional.
(C): Structural.
(D): Change-related.
(Correct): B
(Points): 1
(CF): Usability testing is non-functional testing.
(WF): Usability testing is non-functional testing.
(STARTIGNORE)
(Hint):
(Subject): ISTQB - Sample Test
(Difficulty): Intermediate
(Applicability): course
(ENDIGNORE)


(Type): multiplechoice
(Category): 18
(Grade style): 0
(Random answers): 1
(Question): Which of the following is an example of debugging?
(A): A tester finds a defect and reports it.
(B): A tester retests a fix from the developer and finds a regression.
(C): A developer finds and fixes a defect.
(D): A developer performs unit testing.
(Correct): C
(Points): 1
(CF): C is correct. Debugging is what the developer does to identify the cause of the defect, analyze it and fix it. D may involve debugging, if the developer finds a defect, but the act of unit testing is not the same as debugging.
(WF): C is correct. Debugging is what the developer does to identify the cause of the defect, analyze it and fix it. D may involve debugging, if the developer finds a defect, but the act of unit testing is not the same as debugging.
(STARTIGNORE)
(Hint):
(Subject): ISTQB - Sample Test
(Difficulty): Intermediate
(Applicability): course
(ENDIGNORE)