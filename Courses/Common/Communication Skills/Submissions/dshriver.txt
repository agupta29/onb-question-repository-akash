(STARTIGNORE)

Note: Any text within the (STARTIGNORE) and (ENDIGNORE) tags will be ignored when importing this question template

INSTRUCTIONS
Fill in the blank question templates below then upload and check them under the "Tests -> Question bank -> Import questions" section.
Maximum of 50 questions can be uploaded at once

Your categories numbers are listed below for your reference to add to the (Category): line
For example, the line below will use the 'Generic' category

(Category):

Your categories:
2 Axure
6 Communication
0 Generic
8 HTML
1 Java
3 JIRA
4 OOP
5 SDLC
7 Unix Scripting

(Grade style): Only applicable for 'multiplechoice' (with more then 1 correct answer) style questions
0: Give full score only when all correct responses are provided
1: Give partial score per correct response and deduct partial score for incorrect responses
2: Give partial score per correct response but do not deduct partial score for incorrect responses

(Submitter): This is the name of the person creating and submitting the questions.

(Course Site): This is the site the course was taken on. The options for this are Lynda, and Udemy.

(Course Name): This is the exact name of the course as it is written on the site.

(Course URL): The URL of the course.

(Discipline): This is what discipline the training is classified under, Professional or Technical.

(Hint): This is an optional field that can be used if you wish to provide a hint to the user about the question.

(Subject): This is similar to category but can be used to specify more specific detail on the category.

(Difficulty): This is a range of difficulty of the question, Beginner, Intermediate, and Advanced.

(Applicability): This is if the question is about the course or can be applied to the category in general. Course or General are accepted choices for this section.


(ENDIGNORE)

(STARTIGNORE)
Quiz Details
(Submitter): Derrick Shriver
(Course Site):Udemy
(Course Name): Consulting Skills Series Communication
(Course URL):https://www.udemy.com/consulting-skills-series-communication/?dtcode=NXFwWSz1vMzW
(Discipline): Professional
(ENDIGNORE)

(Type): multiplechoice
(Category): 6
(Grade style): 1
(Random answers): 1
(Question): Which of the following are recommended for effective conference calls?
(A): Use 3x3
(B): Wait 5 min. for the important people
(C): Assume participants read the agenda.
(D): Good phrasing includes "this is my intent for today's call..."
(E): Avoid slides if possible, a document is better.
(F): Publish meeting notes only to those who attended
(Correct): A,D,E
(Points): 1
(CF): 3x3 email the agenda, materials, and do this again the day of the call.  However, assume participants haven't read it.  Meeting notes should go to all invited participants.
(WF): 3x3 email the agenda, materials, and do this again the day of the call.  However, assume participants haven't read it.  Meeting notes should go to all invited participants. 
(STARTIGNORE)
(Hint):
(Subject):Effective Meetings
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)

(Type): truefalse
(Category): 6
(Grade style): 0
(Random answers): 0
(Question): In the consulting context, emails are business documents and should be polite, have limited humor, and be double-checked for form and accuracy before sending.
(A): True
(B): False
(Correct): A
(Points): 1
(CF): 
(WF):  
(STARTIGNORE)
(Hint):
(Subject):Email
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)

(Type): truefalse
(Category): 6
(Grade style): 0
(Random answers): 0
(Question): Simplify your presentations because simpler material is easier to author
(A): True
(B): False
(Correct): B
(Points): 1
(CF): Simpler is generally harder to author.  The reason to aim for simplicity is to help your audience.
(WF): Simpler is generally harder to author.  The reason to aim for simplicity is to help your audience.  
(STARTIGNORE)
(Hint):
(Subject):Presentations
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)

(Type): truefalse
(Category): 6
(Grade style): 0
(Random answers): 0
(Question): When presenting, including stories can be an effective way to connect with your audience
(A): True
(B): False
(Correct): A
(Points): 1
(CF): Presentations include the goal of increasing memory and attentiveness
(WF): Presentations include the goal of increasing memory and attentiveness 
(STARTIGNORE)
(Hint):
(Subject):Presentations
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)

(Type): truefalse
(Category): 6
(Grade style): 0
(Random answers): 0
(Question): Be careful of your thoughts; they may break into words at any time
(A): True
(B): False
(Correct): A
(Points): 1
(CF): 
(WF): 
(STARTIGNORE)
(Hint):
(Subject):Communication
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): truefalse
(Category): 6
(Grade style): 0
(Random answers): 0
(Question): When working with Executives, deliver the bottom line results first.  Then summarize how you got there.
(A): True
(B): False
(Correct): A
(Points): 1
(CF): Time is the executive's most precious asset.
(WF): 
(STARTIGNORE)
(Hint):
(Subject):Communication
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)
