(STARTIGNORE)
Quiz Details
(Submitter): John Arndt
(Course Site): www.lynda.com
(Course Name): SQL Essential Training
(Course URL): http://www.lynda.com/SQL-tutorials/Searching-within-result-set/139988/166309-4.html
(Discipline): Technical
(ENDIGNORE)

(Type): multiplechoice
(Category): 20
(Question): What does the acronym CRUD stand for?
(A): Call, retry, update, deny
(B): Create, read, update, delete
(C): Create, replace, use, delete
(D): Call, read, update, demolish 
(Correct): B
(CF): CRUD is an acronym that stands for Create, Read, Update and Delete. These are the four basic functions of a database systems.
(WF): CRUD is an acronym that stands for Create, Read, Update and Delete. These are the four basic functions of a database systems.
(STARTIGNORE)
(Hint):
(Subject): SQL/Database
(Difficulty): Beginner
(Applicability): General
(ENDIGNORE)


(Type): multiplechoice
(Category): 20
(Question): Given a table named [b]STATES[/b] what statement below would correctly return all rows from the table that have a value in the column [b]STATE_NAME[/b] that start with the letter "N"?
(A): SELECT * FROM STATES WHERE STATE_NAME = 'N%';
(B): SELECT * FROM STATES WHERE STATE_NAME IN ('N');
(C): SELECT * FROM STATES WHERE STATE_NAME LIKE 'N%';
(D): SELECT * FROM STATES WHERE STATE_NAME BETWEEN 'M' and 'O';
(Correct): C
(CF): The LIKE operator is used in a WHERE clause to search for a specified pattern in a column.
(WF): The LIKE operator is used in a WHERE clause to search for a specified pattern in a column.
(STARTIGNORE)
(Hint):
(Subject): SQL/Database
(Difficulty): Intermediate
(Applicability): General
(ENDIGNORE)


(Type): multiplechoice
(Category): 20
(Question): Given the table [b]NAMES[/b] with the following columns [code](ID [b]is not null[/b], F_NAME, L_NAME, MIDDLE_INIT)[/code] what statement below would successfully add the following row to the table (ID=6, F_NAME='Mike', L_NAME='Smith')?
(A): INSERT INTO NAMES (6,'MIKE','SMITH');
(B): INSERT INTO NAMES VALUES (6,'MIKE','SMITH');
(C): INSERT INTO NAMES (ID, F_NAME, L_NAME) VALUES (6, 'Mike', 'Smith');
(D): INSERT INTO NAMES (ID, F_NAME, L_NAME, MIDDLE_INIT) VALUES (6, 'Mike', 'Smith');
(Correct): C
(CF): When inserting data that does not have a value for every column you should specify the columns you are adding values to in the insert statement.
(WF): When inserting data that does not have a value for every column you should specify the columns you are adding values to in the insert statement.
(STARTIGNORE)
(Hint): 
(Subject): SQL/Database
(Difficulty): Intermediate
(Applicability): General
(ENDIGNORE)


(Type): multiplechoice
(Category): 20
(Random answers): 1
(Question): What syntax would be used to create a database table called [b]PETS[/b] with 3 columns [code](ID is an INTEGER, NAME is TEXT, and PTYPE is TEXT)[/code]?
(A): INSERT TABLE PETS (ID INTEGER, NAME TEXT, PTYPE TEXT);
(B): CREATE TABLE PETS (ID INTEGER, NAME TEXT, PTYPE TEXT);
(C): CREATE TABLE PETS WHERE ID INTEGER, NAME TEXT, PTYPE TEXT;
(D): CREATE TABLE PETS "ID INTEGER", "NAME TEXT", "PTYPE TEXT";
(Correct): B
(CF): CREATE TABLE requires all columns to be defined in parenthesis.
(WF): CREATE TABLE requires all columns to be defined in parenthesis.
(STARTIGNORE)
(Hint): 
(Subject): SQL/Database
(Difficulty): Intermediate 
(Applicability): General
(ENDIGNORE)


(Type): multiplechoice
(Category): 20
(Question): What is aggregate data?
(A): Results of an SQL query.
(B): Information derived from more than one row at a time.
(C): The process of copying data from one table to another.
(D): Information regarding time of insert on a specific row.
(Correct): B
(CF): Aggregate data is the data that is the result of applying a process to combine data elements from different sources.
(WF): Aggregate data is the data that is the result of applying a process to combine data elements from different sources.
(STARTIGNORE)
(Hint):
(Subject): SQL/Database
(Difficulty): Beginner
(Applicability): General
(ENDIGNORE)


(Type): multiplechoice
(Category): 20
(Random answers): 1
(Question): Select the aggregate functions. (Select two)
(A): SUM
(B): COUNT
(C): DISTINCT
(D): TO_STRING
(Correct): A, B
(CF): Aggregate functions are functions where the values of multiple rows are grouped together as input on certain criteria to form a single value of more significant meaning or measurement. 
(WF): Aggregate functions are functions where the values of multiple rows are grouped together as input on certain criteria to form a single value of more significant meaning or measurement.
(STARTIGNORE)
(Hint):
(Subject): SQL/Database
(Difficulty): Intermediate
(Applicability): General
(ENDIGNORE)


(Type): multiplechoice
(Category): 20
(Random answers): 1
(Question): How does the HAVING clause differ from the WHERE clause?
(A): There is no difference.
(B): HAVING is for filtering aggregate data; WHERE is for non-aggregate data.
(C): WHERE is for filtering aggregate data; HAVING is for non-aggregate data.
(D): WHERE is used to filter out results; HAVING is used to find additional results.
(Correct): B
(CF): HAVING is for filtering aggregate data; WHERE is for non-aggregate data.
(WF): HAVING is for filtering aggregate data; WHERE is for non-aggregate data.
(STARTIGNORE)
(Hint):
(Subject): SQL/Database
(Difficulty): Beginner
(Applicability): General
(ENDIGNORE)


(Type): multiplechoice
(Category): 20
(Random answers): 1
(Question): What benefits come from combining multiple operations into transactions? (Select three)
(A): Improved performance
(B): Consistency of operations when run multiple times
(C): Transactions force data into tables regardless of constraints
(D): The state of the database will automatically rollback if any portion fails
(E): Incremental saves mean that on a failure only the failed rows need to be fixed
(Correct): A, B, D
(CF): If any of these operations fails, the entire group of operations is treated as failed. And the database is restored to its state before the group of operations is started.
(WF): If any of these operations fails, the entire group of operations is treated as failed. And the database is restored to its state before the group of operations is started.
(STARTIGNORE)
(Hint):
(Subject): SQL/Database
(Difficulty): Beginner
(Applicability): General
(ENDIGNORE)


(Type): multiplechoice
(Category): 20
(Random answers): 1
(Question): What is the correct definition of a trigger?
(A): Named functions that are triggered when called by name in a query statement.
(B): Operations that are automatically performed when a specific database event occurs.
(C): Logging operations that write to transaction logs when values in a database change.
(D): Warning messages that are automatically presented to the user when specific actions are performed.
(Correct): B
(CF): A database trigger is procedural code that is automatically executed in response to certain events on a particular table or view in a database. The trigger is mostly used for maintaining the integrity of the information on the database.
(WF): A database trigger is procedural code that is automatically executed in response to certain events on a particular table or view in a database. The trigger is mostly used for maintaining the integrity of the information on the database.
(STARTIGNORE)
(Hint):
(Subject): SQL/Database
(Difficulty): Intermediate
(Applicability): General
(ENDIGNORE)


(Type): multiplechoice
(Category): 20
(Random answers): 1
(Question): Which syntax below is correct for an update statement?
(A): UPDATE table SET column1 = value WHERE column2 = value2;
(B): UPDATE table WHERE column2 = value2 SET column1 = value;
(C): UPDATE WHERE column2 = value2 SET column1 = value ON table;
(D): UPDATE column1 = value ON TABLE table WHERE column2 = value2;
(Correct): A
(CF): UPDATE table SET column1 = value WHERE column2 = value2;
(WF): UPDATE table SET column1 = value WHERE column2 = value2;
(STARTIGNORE)
(Hint): Beginner
(Subject): SQL/Database
(Difficulty): Beginner
(Applicability): General
(ENDIGNORE)


(Type): multiplechoice
(Category): 20
(Question): A view is which of the following?
(A): A base table that can be accessed via SQL commands
(B): A virtual table that can be accessed via SQL commands
(C): A base table that cannot be accessed via SQL commands
(D): A virtual table that cannot be accessed via SQL commands
(Correct): B
(CF): A view can be thought of as either a virtual table or a stored query. The data accessible through a view is not stored in the database as a distinct object. What is stored in the database is a SELECT statement.
(WF): A view can be thought of as either a virtual table or a stored query. The data accessible through a view is not stored in the database as a distinct object. What is stored in the database is a SELECT statement.
(STARTIGNORE)
(Hint):
(Subject): SQL/Database
(Difficulty): Intermediate
(Applicability): General
(ENDIGNORE)


(Type): multiplechoice
(Category): 20
(Question): SQL data definition commands make up  ________ .
(A): DML
(B): XML
(C): DDL
(D): DDC
(E): HTML
(Correct): C
(CF): DDL stands for Data Definition Language.
(WF): DDL stands for Data Definition Language.
(STARTIGNORE)
(Hint):
(Subject): SQL/Database
(Difficulty): Beginner
(Applicability): General
(ENDIGNORE)


(Type): multiplechoice
(Category): 20
(Question): A subquery in a SQL SELECT statement is enclosed in:
(A): braces -- {...}
(B): CAPITAL LETTERS
(C): brackets -- [...]
(D): parenthesis -- (...) 
(Correct): D
(CF): Subqueries are contained in parenthesis.
(WF): Subqueries are contained in parenthesis.
(STARTIGNORE)
(Hint):
(Subject): SQL/Database
(Difficulty): Beginner
(Applicability): General
(ENDIGNORE)


(Type): multiplechoice
(Category): 20
(Question): What is the proper syntax for combining data from 2 tables? (Select two)
(A): [code]SELECT * FROM table1, table2 ON table1.column1 = table2.column1;[/code]
(B): [code]SELECT * FROM table1, table2 WHERE table1.column1 = table2.column1;[/code]
(C): [code]SELECT * FROM table1 JOIN table2 ON table1.column1 = table2.column1;[/code]
(D): [code]SELECT * FROM table1 JOIN table2 WHERE table1.column1 = table2.column1;[/code]
(Correct): B, C
(CF): Joins are useful for bringing data together from different tables based on their database relations.
(WF): Joins are useful for bringing data together from different tables based on their database relations.
(STARTIGNORE)
(Hint):
(Subject): SQL/Database
(Difficulty): Intermediate
(Applicability): General
(ENDIGNORE)


(Type): multiplechoice
(Category): 20
(Question): A relational database consists of a collection of
(A): Keys
(B): Tables
(C): Fields
(D): Records
(Correct): B
(CF): Fields are the column of the relation or tables.Records are each row in relation.Keys are the constraints in a relation. 
(WF): Fields are the column of the relation or tables.Records are each row in relation.Keys are the constraints in a relation. 
(STARTIGNORE)
(Hint):
(Subject): SQL/Database
(Difficulty): Beginner
(Applicability): General
(ENDIGNORE)


(Type): multiplechoice
(Category): 20
(Question): Knowing the following table definition: [code]Create table employee (id integer,name varchar(20),salary not null);[/code] which of the following insert statements will produce an error?
(A): Insert into employee values (1007, Ross, );
(B): Insert into employee values (1005, Rach, 0);
(C): Insert into employee values (1002, Joey, 335);
(D): Insert into employee values (1006, Marcel, 228);
(Correct): A
(CF): Not null constraint is specified which means some value (can include 0 also) should be given.
(WF): Not null constraint is specified which means some value (can include 0 also) should be given.
(STARTIGNORE)
(Hint):
(Subject): SQL/Database
(Difficulty): Intermediate
(Applicability): General
(ENDIGNORE)


(Type): multiplechoice
(Category): 20
(Question): What command would be used to remove a table from the database?
(A): DROP
(B): REMOVE
(C): DELETE
(D): UPDATE
(Correct): A
(CF): Drop is used to remove a table from the database.
(WF): Drop is used to remove a table from the database.
(STARTIGNORE)
(Hint):
(Subject): SQL/Database
(Difficulty): Beginner
(Applicability): General
(ENDIGNORE)


(Type): multiplechoice
(Category): 20
(Question): To filter out duplicate rows from the results of a SQL SELECT statement, the ________ qualifier specified must be included.
(A): ONLY
(B): SINGLE
(C): SPECIFIC
(D): DISTINCT
(E): MINIMIZE
(Correct): D
(CF): The SQL DISTINCT command used along with the SELECT keyword retrieves only unique data entries depending on the column list you have specified after it.
(WF): The SQL DISTINCT command used along with the SELECT keyword retrieves only unique data entries depending on the column list you have specified after it.
(STARTIGNORE)
(Hint):
(Subject): SQL/Database
(Difficulty): Intermediate
(Applicability): General
(ENDIGNORE)


(Type): multiplechoice
(Category): 20
(Question): Which one of the following sorts rows in SQL?
(A): SORT BY
(B): ALIGN BY
(C): ORDER BY
(D): GROUP BY
(Correct): C
(CF): The ORDER BY keyword is used to sort the result-set by one or more columns.
(WF): The ORDER BY keyword is used to sort the result-set by one or more columns.
(STARTIGNORE)
(Hint):
(Subject): SQL/Database
(Difficulty): Intermediate
(Applicability): General
(ENDIGNORE)


(Type): multiplechoice
(Category): 20
(Question): What is the purpose of indexing in SQL?
(A): Indexes are used to assist in special cases.
(B): Indexes exist solely to improve query speed.
(C): Indexes rarely make a difference in SQL.
(D): Indexes are used to make table storage more efficient.
(Correct): B
(CF): The sole purpose of indexes in any relational database management system is to reduce the access time for SQL statements.
(WF): The sole purpose of indexes in any relational database management system is to reduce the access time for SQL statements.
(STARTIGNORE)
(Hint):
(Subject): SQL/Database
(Difficulty): 
(Applicability): General
(ENDIGNORE)


(Type): multiplechoice
(Category): 20
(Question): A UNION query is which of the following?
(A): Combines the output from multiple queries and must include the same number of columns.
(B): Combines the output from multiple queries and does not include the same number of columns.
(C): Combines the output from no more than two queries and must include the same number of columns.
(D): Combines the output from no more than two queries and does not include the same number of columns.
(Correct): A
(CF): The UNION operator is used to combine the result-set of two or more SELECT statements. Notice that each SELECT statement within the UNION must have the same number of columns.
(WF): The UNION operator is used to combine the result-set of two or more SELECT statements. Notice that each SELECT statement within the UNION must have the same number of columns.
(STARTIGNORE)
(Hint):
(Subject): SQL/Database
(Difficulty): Intermediate
(Applicability): General
(ENDIGNORE)


(Type): multiplechoice
(Category): 20
(Question): Which of the following is true about the SQL query given below?
[code]
SELECT col1, col2
FROM tab1
WHERE col1 = 'A'
ORDER BY col2 DESC, col1;
[/code]
(A): It will display the row which has the col1 value as 'A' ordered by the col1 in the descending order and then col2 in the descending order.
(B): The above query will be sorted in descending order on the basis of col2 only and the use of col1 in the ORDER BY clause will be discarded.
(C): The ORDER BY clause will not work as the keyword DESC should be always written in the end of the ORDER BY clause and not in between as given in the query.
(D): It will display the row which has the col1 value as 'A' ordered by the col1 and then followed by col2 as the execution of the ORDER BY clause happens from the order of columns in the SELECT statement.
(Correct): B
(CF):  Since the COL1 is already filtered and fixed in the query as a scalar value, no sorting will happen on the basis of COL1.
(WF):  Since the COL1 is already filtered and fixed in the query as a scalar value, no sorting will happen on the basis of COL1.
(STARTIGNORE)
(Hint):
(Subject): SQL/Database
(Difficulty): Intermediate
(Applicability): General
(ENDIGNORE)


(Type): multiplechoice
(Category): 20
(Question):  Select [b]two[/b] equivalent answers for (Select two)
				[code]WHERE S_CID BETWEEN '7000' AND '8000'[/code]
(A): WHERE S_CID > '7000' AND S_CID < '8000'
(B): WHERE S_CID >= '7000' AND S_CID < '8000'
(C): WHERE S_CID > '7000' AND S_CID <= '8000'
(D): WHERE S_CID >= '7000' AND S_CID <= '8000'
(E): WHERE S_CID < '8000' AND NOT S_CID < '7000'
(F): WHERE S_CID > '7000' AND NOT S_CID >= '8000'
(G): WHERE S_CID <= '8000' AND NOT S_CID < '7000'
(E): WHERE S_CID >= '7000' AND NOT S_CID >= '8000'
(Correct): D,G
(CF): The values create an inclusive range that the expression is compared to.
(WF): The values create an inclusive range that the expression is compared to.
(STARTIGNORE)
(Hint):
(Subject): SQL/Database
(Difficulty): Intermediate
(Applicability): General
(ENDIGNORE)


(Type): multiplechoice
(Category): 20
(Question): The CASE expression in SQL can be used to provide what?
(A): For loop logic
(B): While loop logic
(C): If-then-else type logic
(D): Static variable assignment logic
(Correct): C
(CF): CASE is used to provide if-then-else type of logic to SQL.
(WF): CASE is used to provide if-then-else type of logic to SQL.
(STARTIGNORE)
(Hint):
(Subject): SQL/Database
(Difficulty): Intermediate
(Applicability): General
(ENDIGNORE)


(Type): multiplechoice
(Category): 20
(Question): The result of a SQL SELECT statement is a(n) ________ .
(A): form
(B): file
(C): table
(D): report
(Correct): C
(CF): SELECT statements return information in the form of a table.
(WF): SELECT statements return information in the form of a table.
(STARTIGNORE)
(Hint):
(Subject): SQL/Database
(Difficulty): Beginner
(Applicability): General
(ENDIGNORE)


(Type): multiplechoice
(Category): 20
(Question): Find the SQL statement below that is equal to the following: [code]SELECT NAME FROM CUSTOMER WHERE STATE = 'VA';[/code]
(A): SELECT NAME IN CUSTOMER WHERE STATE = 'VA';
(B): SELECT NAME FROM CUSTOMER WHERE STATE = 'V';
(C): SELECT NAME IN CUSTOMER WHERE STATE IN ('VA');
(D): SELECT NAME FROM CUSTOMER WHERE STATE IN ('VA');
(Correct): D
(CF): FROM is used to list the tablenames to gather data from; IN gives you the ability to give multiple options in parenthesis separated by commas.
(WF): FROM is used to list the tablenames to gather data from; IN gives you the ability to give multiple options in parenthesis separated by commas.
(STARTIGNORE)
(Hint):
(Subject): SQL/Database
(Difficulty): Beginner
(Applicability): General
(ENDIGNORE)


(Type): multiplechoice
(Category): 20
(Question): Which of the following contains a complete record of all activity that affected the contents of a database during a certain period of time?
(A): Report writer
(B): Query Language
(C): Transaction log
(D): Data manipulation language
(Correct): C
(CF): A transaction log is a history of actions executed by a database management system to guarantee ACID properties over crashes or hardware failures.
(WF): A transaction log is a history of actions executed by a database management system to guarantee ACID properties over crashes or hardware failures.
(STARTIGNORE)
(Hint):
(Subject): SQL/Database
(Difficulty): 
(Applicability): General
(ENDIGNORE)


(Type): multiplechoice
(Category): 20
(Question): Which of the following fields in a student file can be used as a primary key?
(A): GPA
(B): Major
(C): Minor
(D): Class
(E): Social Security Number
(Correct): E
(CF): Social Security would be unique to each student so it could be a primary key.
(WF): Social Security would be unique to each student so it could be a primary key.
(STARTIGNORE)
(Hint):
(Subject): SQL/Database
(Difficulty): Beginner
(Applicability): General
(ENDIGNORE)


(Type): multiplechoice
(Category): 20
(Question): The SQL statement [code]SELECT SUBSTR('123456789', INSTR('abcabcabc','b'), 4) FROM EMP;[/code] prints
(A): 6789
(B): 2345
(C): 1234
(D): 456789
(E): 123456
(Correct): B
(CF): The INSTR expression will provide the first index of 'b' which will be used as the starting point for the substring which will go to the 4th index.
(WF): The INSTR expression will provide the first index of 'b' which will be used as the starting point for the substring which will go to the 4th index.
(STARTIGNORE)
(Hint):
(Subject): SQL/Database
(Difficulty): Advanced
(Applicability): General
(ENDIGNORE)


(Type): multiplechoice
(Category): 20
(Question): Which of the following statements contains an error?
(A): Select empid from emp;
(B): Select * from emp where empid = 10003;
(C): Select empid from emp where empid = 10006;
(D): Select empid where empid = 1009 and lastname = ‘GELLER’;
(Correct): D
(CF): D does not have a FROM clause which specifies the relation from which the values has to be selected.
(WF): D does not have a FROM clause which specifies the relation from which the values has to be selected.
(STARTIGNORE)
(Hint):
(Subject): SQL/Database
(Difficulty): Advanced
(Applicability): General
(ENDIGNORE)