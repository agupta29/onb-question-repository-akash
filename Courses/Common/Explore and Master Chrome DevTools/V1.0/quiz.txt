(STARTIGNORE)

Note: Any text within the (STARTIGNORE) and (ENDIGNORE) tags will be ignored when importing this question template

INSTRUCTIONS
Fill in the blank question templates below then upload and check them under the "Tests -> Question bank -> Import questions" section.
Maximum of 50 questions can be uploaded at once

Your categories numbers are listed below for your reference to add to the (Category): line
For example, the line below will use the 'Generic' category

(Category):

Your categories:
0 Generic
1 Java
2 Axure
3 JIRA
4 OOP
5 SDLC
6 Communication
7 Unix Scripting
8 HTML
9 CSS
10 JavaScript
11 Angular JS
12 Design Patterns
13 Git
14 Java Design Pattern
15 Business Analysis
16 Version Control
17 Presentations
18 Software Testing
19 Foundations of Programming: Software Quality Assurance
20 PowerPoint 2013 Essential Training
21 SQL Essential Training
22 Web Design Fundamentals

(Grade style): Only applicable for 'multiplechoice' (with more than 1 correct answer) style questions
0: Give full score only when all correct responses are provided
1: Give partial score per correct response and deduct partial score for incorrect responses
2: Give partial score per correct response but do not deduct partial score for incorrect responses

(Submitter): This is the name of the person creating and submitting the questions.

(Course Site): This is the site the course was taken on. The options for this are Lynda, and Udemy.

(Course Name): This is the exact name of the course as it is written on the site.

(Course URL): The URL of the course.

(Discipline): This is what discipline the training is classified under, Professional or Technical.

(Hint): This is an optional field that can be used if you wish to provide a hint to the user about the question.

(Subject): This is similar to category but can be used to specify more specific detail on the category.

(Difficulty): This is a range of difficulty of the question, Beginner, Intermediate, and Advanced.

(Applicability): This is if the question is about the course or can be applied to the category in general. Course or General are accepted choices for this section.


(ENDIGNORE)

(STARTIGNORE)
Quiz Details
(Submitter): Victor Oyedeji
(Course Site): discover-devtools.codeschool.com
(Course Name): Explore and Master Chrome DevTools
(Course URL): http://discover-devtools.codeschool.com/
(Discipline): Technical
(ENDIGNORE)

(Type): multiplechoice
(Category): 22
(Question): Which are two ways to select an element using the DOM Editor? (Choose two)
(A): Right click > Inspect Element
(B): Hovering over an HTML element in the DOM Editor
(C): Perform a search function for the element using the DOM Editor
(D): Opening the DOM Editor and double-clicking the element on the web page
(Correct): A,B
(WF): Right click > Inspect Element and hovering over an HTML element are two ways to select an element using the DOM Editor
(STARTIGNORE)
(Hint):
(Subject): Explore and Master Chrome DevTools
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 22
(Question): What would the following expression do when entered into the Chrome DevTools console? [code] $0[/code]
(A): It will print out the last selected element
(B): It will print out the first selected element
(C): It will print out any element with an id of '0'
(D): It will print out any element with a class of '0'
(E): It will print out all elements with a name of '0'
(Correct): A
(WF): The code will print out the last selected element.
(STARTIGNORE)
(Hint):
(Subject): Explore and Master Chrome DevTools
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE) 


(Type): multiplechoice
(Category): 22
(Question): To make CSS changes on a web page, which panel should be open in the DOM Editor?
(A): Styles
(B): Sources
(C): Elements
(D): Properties
(Correct): A
(WF): The Styles tab is the correct tab to make CSS changes in the DOM Editor
(STARTIGNORE)
(Hint):
(Subject): Explore and Master Chrome DevTools
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 22
(Question): When is it a good idea to load JavaScript files before rendering the page?
(A): When the HTML is large
(B): When the JavaScript files are large
(C): When the JavaScript files are small
(D): When the JavaScript files affect the layout of the page
(E): When the JavaScript files affect the behavior of the page
(Correct): D
(WF): It is a good idea to load the JavaScript files before rendering the page when they affect the layout of the page.
(STARTIGNORE)
(Hint):
(Subject): Explore and Master Chrome DevTools
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)

(Type): multiplechoice
(Category): 22
(Question): Which is [b]not[/b] a valid CSS styles state in the DOM Editor?
(A): Focus
(B): Hover
(C): Visited
(D): Selected
(Correct): D
(WF): Select is not a valid CSS styles state that can be activated in the DOM Editor
(STARTIGNORE)
(Hint):
(Subject): Explore and Master Chrome DevTools
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 22
(Question): If requested resources are labelled as cached what is true about those resources? 
(A): They were downloaded from a CDN in the Chrome Console
(B): They were downloaded from the server in the Chrome Console
(C): The local resources were identical, checked against the server's resource list, and the cached version was used 
(D): The resources contained cache information needed to check the CDN inside of the Chrome Console, which also controlled the cached version
(Correct): C
(WF): Before a resource is downloaded the computer checks to see if it already has the file, if it does the cached version is used.
(STARTIGNORE)
(Hint):
(Subject): Explore and Master Chrome DevTools
(Difficulty): Beginner
(Applicability): General
(ENDIGNORE)

(Type): multiplechoice
(Category): 22
(Question): When a resource has a '404' error, what does this mean?
(A): The resource failed to be requested properly
(B): The resource was no longer needed by the server
(C): The resource could not be found at the location specified
(D): The resource contents did not match what the server expected
(E): The resource took too long to load and the connection timed out
(Correct): C
(WF): A resource with a '404' error indicates that the resource could not be found at the locations specified.
(STARTIGNORE)
(Hint):
(Subject): Explore and Master Chrome DevTools
(Difficulty): Beginner
(Applicability): General
(ENDIGNORE)

(Type): multiplechoice
(Category): 22
(Question): When analyzing page performance, what effect does low 'FPS' have? (Choose two)
(A): It prevents the page from loading
(B): It causes the page to seem unresponsive
(C): It breaks all button and form functionality
(D): It causes animations and transitions to become 'jumpy' or no longer smooth
(Correct): B,D
(WF): When a page has low 'FPS' it will seem unresponsive and animations/transitions will become 'jumpy' or no longer smooth.
(STARTIGNORE)
(Hint):
(Subject): Explore and Master Chrome DevTools
(Difficulty): Intermediate
(Applicability): General
(ENDIGNORE) 


(Type): multiplechoice
(Category): 22
(Question): Which button is used to add a style attribute to an element in the DOM Editor?
(A): The magnifying glass
(B): The '+' button in the editor
(C): The Elements tab in the editor
(D): The 'x' button on the far right corner
(Correct): B 
(WF): The '+' button in the editor signifies a style attribute addition in the DOM editor.
(STARTIGNORE)
(Hint):
(Subject): Explore and Master Chrome DevTools
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE) 


(Type): multiplechoice
(Category): 22
(Question): Which tab is used to write JavaScript functions?
(A): Sources
(B): Console
(C): Elements
(D): Resources
(Correct): B
(WF): The Console tab is used to write JavaScript functions in the DOM Editor
(STARTIGNORE)
(Hint):
(Subject): Explore and Master Chrome DevTools
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE) 


(Type): multiplechoice
(Category): 22
(Question): Which commands are needed to debug an application inside the Chrome Console?
(A): [code].log [/code] commands
(B): [code].event [/code] commands
(C): [code].debug [/code] commands
(D): [code].listener [/code] commands
(Correct): A
(WF): The [code].log [/code] commands is used in debugging applications inside the Chrome Console
(STARTIGNORE)
(Hint):
(Subject): Explore and Master Chrome DevTools
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE) 

(Type): multiplechoice
(Category): 22
(Question): Which element selector shortcut replaces the [code]document.query selector()[/code] call in the DOM Editor?
(A): $
(B): #
(C): %
(D): <>
(Correct): A
(WF): The $ element selector (also known as Bling) replaces the [code]document.query selector()[/code] call in the DOM Editor.
(STARTIGNORE)
(Hint):
(Subject): Explore and Master Chrome DevTools
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 22
(Question): What happens when you load the jQuery Bling to the Chrome Console?
(A): The console suddenly shuts down
(B): You can choose to use any Bling of your choice
(C): It overrides the console's Bling with the jQuery's Bling
(D): Nothing. The console's Bling will override the jQuery's Bling
(Correct): C
(WF): Loading the jQuery Bling to the Chrome's Console overrides Chrome's Bling with the jQuery Bling
(STARTIGNORE)
(Hint):
(Subject): Explore and Master Chrome DevTools
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE) 


(Type): multiplechoice
(Category): 22
(Question): The Chrome Console will _________ any error their web application throws.
(A): Delete
(B): Accept
(C): Log
(D): Override
(Correct): C
(WF): The Chrome Console will log out any error their web application throws.
(STARTIGNORE)
(Hint):
(Subject): Explore and Master Chrome DevTools
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 22
(Question): Which call is used to remove data from the Chrome Console?
(A): [code].clear[/code]
(B): [code].delete[/code]
(C): [code].destroy[/code]
(D): [code].remove[/code]
(Correct): D
(WF): The [code].remove[/code] call is used to remove data from the Chrome Console
(STARTIGNORE)
(Hint):
(Subject): Explore and Master Chrome DevTools
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)

(Type): multiplechoice
(Category): 22
(Question): Which two are good reasons to use the "pause on exceptions" icon when creating an event in the Chrome Console? (Choose two)
(A): To make a variable "null"
(B): To examine the jQuery library
(C): To show the value on a variable
(D): To confirm an error in the code
(Correct): C,D
(WF): The "pause on exceptions" icon is used to show the value on a variable and to confirm an error in the code
(STARTIGNORE)
(Hint):
(Subject): Explore and Master Chrome DevTools
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 22
(Question): How can you make the console's debugger pause [b]only[/b] on "uncaught exceptions"?
(A): Add a [code]try/catch[/code] block in the JavaScript code
(B): Include the [code].debugger[/code] method to the console
(C): Include the [code]uncaught exception[/code] method to the console
(D): Click the "pause on exceptions" button a second time, then reload the page
(Correct): D
(WF): Clicking the "pause on exceptions" button a second time, and then reloading the page makes the console's debugger skip the "caught exceptions", only to pause on "uncaught exceptions".
(STARTIGNORE)
(Hint):
(Subject): Explore and Master Chrome DevTools
(Difficulty): Advanced
(Applicability): Course
(ENDIGNORE)

(Type): multiplechoice
(Category): 22
(Question): Which panel can you find the local storage in the Chrome Console?
(A): Profile
(B): Network
(C): Elements
(D): Resources
(Correct): D
(WF): The Resources panel shows the local storage of your Chrome Console
(STARTIGNORE)
(Hint):
(Subject): Explore and Master Chrome DevTools
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)

(Type): multiplechoice
(Category): 22
(Question): What purpose does the Timeline panel serve in the Chrome Tools editor?
(A): Identifying network response codes
(B): Identifying the "status code" for each request
(C): Identifying "bottlenecks" in your network requests
(D): Identifying the resources in the HTML and CSS functions
(Correct): C
(WF): Identifying "bottlenecks" in your network request is the purpose the Timeline panel serves.
(STARTIGNORE)
(Hint):
(Subject): Explore and Master Chrome DevTools
(Difficulty): Intermediate
(Applicability): General
(ENDIGNORE) 


(Type): multiplechoice
(Category): 22
(Question): How can you refresh [b]all[/b] of the resources needed to specify the correct load response times for a web page?
(A): Hold [code]shift[/code] and refresh the page
(B): Send a webserver request through the Dev Tools editor
(C): Refresh the page and view the results in the Resources panel
(D): Simply refresh the web page and view the results in the Network panel
(Correct): A
(WF): Holding [code]shift[/code] and refreshing the page refreshes all of the resources needed to specify the correct load response times for a web page
(STARTIGNORE)
(Hint):
(Subject): Explore and Master Chrome DevTools
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE) 


(Type): multiplechoice
(Category): 22
(Question): Why is having a [code]304 status code[/code] [b]not[/b] a good practice for viewing web response times?
(A): It signifies a server error in the Chrome Console
(B): It deletes the cache files, therefore not showing a good representation of web response times
(C): It stores the cache files from a previous upload, so all resources will not load on a page refresh
(D): It stores the cache files from a previous upload, therefore slowing down response times on a web refresh
(Correct): D
(WF): Having a [code]304 status code[/code] stores the cache files from a previous upload, therefore slowing down response times on a web refresh
(STARTIGNORE)
(Hint):
(Subject): Explore and Master Chrome DevTools
(Difficulty): Advanced
(Applicability): Course
(ENDIGNORE) 


(Type): multiplechoice
(Category): 22
(Question): Which Dev Tools extension is great for analyzing the web upload times, as well as providing suggestions for faster response times?
(A): PageSpeed
(B): SpeedTools
(C): CatchAnalyser
(D): SpeedAnalyser
(Correct): A
(WF): The PageSpeed extension is great for analyzing the web upload times, as well as providing suggestions for faster response times
(STARTIGNORE)
(Hint):
(Subject): Explore and Master Chrome DevTools
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE) 



(Type): multiplechoice
(Category): 22
(Question): Which attribute is used to put off parsing the JavaScript, so the web page can be rendered sooner?
(A): Add [code]cache[/code] to the <head> tag
(B): Add [code]parse[/code] to the <script> tag
(C): Add [code]async[/code] to the <script> tag
(D): Add [code]delete[/code] to the <script> tag
(Correct): C
(WF): Adding the [code]async[/code] attribute to the <script> tag is used to put off parsing the JavaScript, so the web page can be rendered sooner
(STARTIGNORE)
(Hint):
(Subject): Explore and Master Chrome DevTools
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)



(Type): multiplechoice
(Category): 22
(Question): Which is a good indicator of a memory leak?
(A): When a page performance slows down after long usage
(B): When image pixels on the page disappears upon page upload
(C): When a [code]400 error[/code] occurs after clicking a link on the page
(D): When images on a page does not display the correct size as originally intended
(Correct): A
(WF): Page performance slowing down after long usage is a good indicator of a memory leak
(STARTIGNORE)
(Hint):
(Subject): Explore and Master Chrome DevTools
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE) 


(Type): multiplechoice
(Category): 22
(Question): What is the main purpose of the CPU Profiler in the Chrome Console?
(A): It can quickly alert you issues in your CPU
(B): It gives a profile of the CPU which has memory leaks
(C): It allows you to parse the <script> tag in order to optimize web performance
(D): It helps you track down exactly where your slow JavaScript's code is occurring
(Correct): C
(WF): The main purpose of the CPU Profiler in the Chrome Console is to help you track down exactly where your slow JavaScript's code is occurring
(STARTIGNORE)
(Hint):
(Subject): Explore and Master Chrome DevTools
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)

(Type): multiplechoice
(Category): 22
(Question): Why are Heap Snapshots essential in the Chrome Console?
(A): Used to take snapshots of hidden CSS attributes
(B): They show performance lag times from the Network tab
(C): They are used to inform web developers of performance issues
(D): They are used to compare snapshots, and to pinpoint memory leak locations
(Correct): D
(WF): Heap Snapshots are essential because they are used to compare snapshots, used to pinpoint memory leak locations in your web page.
(STARTIGNORE)
(Hint):
(Subject): Explore and Master Chrome DevTools
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)

