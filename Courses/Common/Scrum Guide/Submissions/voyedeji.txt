(STARTIGNORE)

Note: Any text within the (STARTIGNORE) and (ENDIGNORE) tags will be ignored when importing this question template

INSTRUCTIONS
Fill in the blank question templates below then upload and check them under the "Tests -> Question bank -> Import questions" section.
Maximum of 50 questions can be uploaded at once

Your categories numbers are listed below for your reference to add to the (Category): line
For example, the line below will use the 'Generic' category

(Category):

Your categories:
0 Generic
1 Java
2 Axure
3 JIRA
4 OOP
5 SDLC
6 Communication
7 Unix Scripting
8 HTML
9 CSS
10 JavaScript
11 Angular JS
12 Design Patterns
13 Git
14 Java Design Pattern
15 Business Analysis
16 Version Control
17 Presentations
18 Software Testing
19 Foundations of Programming: Software Quality Assurance
20 PowerPoint 2013 Essential Training
21 SQL Essential Training
22 Web Design Fundamentals

(Grade style): Only applicable for 'multiplechoice' (with more then 1 correct answer) style questions
0: Give full score only when all correct responses are provided
1: Give partial score per correct response and deduct partial score for incorrect responses
2: Give partial score per correct response but do not deduct partial score for incorrect responses

(Submitter): This is the name of the person creating and submitting the questions.

(Course Site): This is the site the course was taken on. The options for this are Lynda, and Udemy.

(Course Name): This is the exact name of the course as it is written on the site.

(Course URL): The URL of the course.

(Discipline): This is what discipline the training is classified under, Professional or Technical.

(Hint): This is an optional field that can be used if you wish to provide a hint to the user about the question.

(Subject): This is similar to category but can be used to specify more specific detail on the category.

(Difficulty): This is a range of difficulty of the question, Beginner, Intermediate, and Advanced.

(Applicability): This is if the question is about the course or can be applied to the category in general. Course or General are accepted choices for this section.


(ENDIGNORE)

(STARTIGNORE)
Quiz Details
(Submitter): Victor Oyedeji		
(Course Site): http://www.scrumguides.org
(Course Name): Scrum Guide
(Course URL): http://www.scrumguides.org/docs/scrumguide/v1/Scrum-Guide-US.pdf
(Discipline): Professional
(ENDIGNORE)

(Type): truefalse
(Category): 5
(Grade style): 0
(Random answers): 0
(Question): The individuals authorized to cancel a Sprint are both the Product Owner and the Scrum Master.
(A): True
(B): False 
(Correct): B
(Points): 1
(CF): Correct. Only the Product Owner is authorized to cancel a Sprint.
(WF): False. Only the Product Owner is authorized to cancel a Sprint.
(STARTIGNORE)
(Hint):
(Subject): Question Subject
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): truefalse
(Category): 5
(Grade style): 0
(Random answers): 0
(Question): Scrum is difficult to understand, but simple to master.
(A): True	
(B): False
(Correct): B
(Points): 1
(CF): Scrum is actually SIMPLE to understand, but Difficult to master (although we know you wouldn't have a problem either way).
(WF): Scrum is actually SIMPLE to understand, but Difficult to master (although we know you wouldn't have a problem either way).
(STARTIGNORE)
(Hint):
(Subject): Question Subject
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)

(Type): truefalse
(Category): 5
(Grade style): 0
(Random answers): 0
(Question): Only the Scrum Master can tell the Development Team how to turn Product Backlog into potential releasable functionality.
(A): True
(B): False
(Correct): B
(Points): 1
(CF): Correct.  Development Teams are self-organizing.  No one (not even the Scrum Master) tells them how to turn Product Backlog into potential releasable functionality.
(WF): False.  Development Teams are self-organizing.  No one (not even the Scrum Master) tells them how to turn Product Backlog into potential releasable functionality.
(STARTIGNORE)
(Hint):
(Subject): Question Subject
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 5
(Grade style): 0
(Random answers): 1
(Question): _______________ is time-boxed to a maxiumum 8 hours for a one-month Sprint.
(A): Iterations
(B): The Sprint
(C): Daily Scrum
(D): Sprint Planning
(E): Sprint Retrospective
(Correct): D
(Points): 1
(CF): Correct. The Sprint Planning is time-boxed to a maxiumum 8 hours for a one-month Sprint.
(WF): The Sprint Planning is time-boxed to a maxiumum 8 hours for a one-month Sprint.
(STARTIGNORE)
(Hint):
(Subject): Question Subject
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)



(Type): multiplechoice
(Category): 5
(Grade style): 1
(Random answers): 1
(Question): During a Daily Scrum (Standup), what does each member of the Development Team explain? (Select 3)
(A): What did I do yesterday that helped meet the Sprint Goal.
(B): What did I do at the beginning of the week that helped meet the Sprint Goal.		
(C): Do I see any impediment that prevents me or the Development Team from meeting the Sprint Goal.
(D): What will I do today to help met the Spring Goal.
(E): What can I do to help bring in additional team members.
(Correct): A,C,D
(Points): 3
(CF): A, C, and D are correct
(WF): A, C, and D are correct
(STARTIGNORE)
(Hint):
(Subject): Question Subject
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): truefalse
(Category): 5
(Grade style): 0
(Random answers): 0
(Question): The Product Owner will usually speak up about everyday tasks during the Daily Scrum.
(A): True
(B): False
(Correct): B
(Points): 1
(CF): Correct.  The Product Owner will NOT usually speak up about everyday tasks during the Daily Scrum.  His/her job is to manage the Product Backlog.  The Development Team usually talks about everyday tasks.
(WF): False.  The Product Owner will NOT usually speak up about everyday tasks during the Daily Scrum.  His/her job is to manage the Product Backlog.  The Development Team usually talks about everyday tasks.
(STARTIGNORE)
(Hint):
(Subject): Question Subject
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 5
(Grade style): 0
(Random answers): 1
(Question): Which event provides an opportunity for the Scrum Team to inspect itself and create an improvement plan for next Sprint?
(A): Sprint Planning		
(B): Sprint Review
(C): Daily Scrum
(D): Product Owner Meeting
(E): Sprint Retrospective
(Correct): E
(Points): 1
(CF): Correct. The Sprint Retrospective meeting is held for the Scrum Team to inspect itself and create an improvement plan for next Sprint.
(WF): The Sprint Retrospective meeting is held for the Scrum Team to inspect itself and create an improvement plan for next Sprint.
(STARTIGNORE)
(Hint):
(Subject): Question Subject
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)



(Type): multiplechoice
(Category): 5
(Grade style): 1
(Random answers): 1
(Question): What are the main roles of the Scrum Master during a Daily scrum? (Select 2)
(A): Teaches the Development Team to keep The Daily Scrum within the 15-min window
(B): Conducts the Daily Scrum meeting
(C): Enforces the rule that only Development Team members participate in the Daily Scrum
(D): Answers the questions Development Team members may have on a particular daily task issue
(E): Defines the "Definition of Done"
(Correct): A,C
(Points): 2
(CF): Correct. Both A and C are correct.
(WF): Both A and C are correct.
(STARTIGNORE)
(Hint):
(Subject): Question Subject
(Difficulty): Advanced
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 5
(Grade style): 0
(Random answers): 0
(Question): Who decides how long it will take for a Product (or Sprint) Backlog item to build into a "Done" product increment?
(A): Scrum Master
(B): Product Owner
(C): Manager
(D): Development team
(E): All of the above
(Correct): D
(Points): 1
(CF): Correct.  Only The Development Team decides how long it will take for a Product (or Sprint) functionality to build into a "Done" product increment.
(WF): Only The Development Team decides how long it will take for a Product (or Sprint) functionality to build into a "Done" product increment.
(STARTIGNORE)
(Hint):
(Subject): Question Subject
(Difficulty): Advanced
(Applicability): Course
(ENDIGNORE)


(Type): truefalse	
(Category): 5
(Grade style): 0
(Random answers): 0
(Question): Product Backlog refinement usally consumes no more than Ten Percent of the capacity of the Development Team.
(A): True
(B): False
(Correct): A
(Points): 1
(CF): Correct.  Product Backlog refinement usally consumes no more than 10% of the capacity of the Development Team.
(WF): True.  Product Backlog refinement usally consumes no more than 10% of the capacity of the Development Team.
(STARTIGNORE)
(Hint):
(Subject): Question Subject
(Difficulty): Advanced
(Applicability): Course
(ENDIGNORE)
