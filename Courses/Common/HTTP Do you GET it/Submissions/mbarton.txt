(STARTIGNORE)

Note: Any text within the (STARTIGNORE) and (ENDIGNORE) tags will be ignored when importing this question template

INSTRUCTIONS
Fill in the blank question templates below then upload and check them under the "Tests -> Question bank -> Import questions" section.
Maximum of 50 questions can be uploaded at once

Your categories numbers are listed below for your reference to add to the (Category): line
For example, the line below will use the 'Generic' category

(Category):

Your categories:
0 Generic
1 Java
2 Axure
3 JIRA
4 OOP
5 SDLC
6 Communication
7 Unix Scripting
8 HTML
9 CSS
10 JavaScript
11 Angular JS
12 Design Patterns
13 Git
14 Java Design Pattern
15 Business Analysis
16 Version Control
17 Presentations
18 Software Testing
19 Foundations of Programming: Software Quality Assurance
20 PowerPoint 2013 Essential Training
21 SQL Essential Training
22 Web Design Fundamentals
23 Programming Foundations: Fundamentals
24 Java SE 8 Bootcamp
25 RESTful API Testing with Postman
26 Experience Design Patterns in Java
27 Java Web Services
28 Windows Tips & Tricks
29 The Internet & World Wide Web

(Grade style): Only applicable for 'multiplechoice' (with more then 1 correct answer) style questions
0: Give full score only when all correct responses are provided
1: Give partial score per correct response and deduct partial score for incorrect responses
2: Give partial score per correct response but do not deduct partial score for incorrect responses

(Submitter): This is the name of the person creating and submitting the questions.

(Course Site): This is the site the course was taken on. The options for this are Lynda, and Udemy.

(Course Name): This is the exact name of the course as it is written on the site.

(Course URL): The URL of the course.

(Discipline): This is what discipline the training is classified under, Professional or Technical.

(Hint): This is an optional field that can be used if you wish to provide a hint to the user about the question.

(Subject): This is similar to category but can be used to specify more specific detail on the category.

(Difficulty): This is a range of difficulty of the question, Beginner, Intermediate, and Advanced.

(Applicability): This is if the question is about the course or can be applied to the category in general. Course or General are accepted choices for this section.


(ENDIGNORE)

(STARTIGNORE)
Quiz Details
(Submitter): Megan Barton
(Course Site): Team Treehouse
(Course Name): HTTP: Do you GET it?
(Course URL): https://teamtreehouse.com/library/http-do-you-get-it
(Discipline): Technical
(ENDIGNORE)

(Type): multiplechoice
(Category): 29 
(Grade style): 0
(Random answers): 0
(Question): True or False: HTTP remembers your state.
(A): True
(B): False
(Correct): B
(Points): 1
(CF): Correct! Web servers are stateless and treat each new request as if it was the first.
(WF): That's not right. Web servers are stateless and treat each new request as if it was the first.
(STARTIGNORE)
(Hint):
(Subject): HTTP
(Difficulty): Beginner 
(Applicability): General 
(ENDIGNORE)


(Type): multiplechoice
(Category): 29
(Grade style): 2
(Random answers): 1
(Question): Which of the following are valid types of HTTP requests?
(A): GET
(B): PUT
(C): SEND
(D): POST
(E): DELETE
(Correct): A,B,D,E
(Points): 1
(CF): 
(WF): 
(STARTIGNORE)
(Hint):
(Subject): HTTP
(Difficulty): Beginner 
(Applicability): General 
(ENDIGNORE)


(Type): multiplechoice
(Category): 29
(Grade style): 0
(Random answers): 1
(Question): HTTP headers are sent as
(A): Key/value pairs
(B): Cookies
(C): Packets 
(Correct): A
(Points): 1
(CF): 
(WF): 
(STARTIGNORE)
(Hint):
(Subject): HTTP
(Difficulty): Beginner 
(Applicability): General 
(ENDIGNORE)


(Type): multiplechoice
(Category): 29
(Grade style): 0
(Random answers): 1
(Question): What is the latest version of HTTP?
(A): 0.1
(B): 1.0
(C): 1.1
(D): 1.2
(E): 2.1
(Correct): C
(Points): 1
(CF): 
(WF): 
(STARTIGNORE)
(Hint):
(Subject): HTTP
(Difficulty): Beginner 
(Applicability): General 
(ENDIGNORE)


(Type): multiplechoice
(Category): 29
(Grade style): 0
(Random answers): 0
(Question): True or False: when sending an HTTP request, before a DNS server is contacted your local DNS cache will be checked?
(A): True
(B): False
(Correct): A
(Points): 1
(CF): 
(WF): 
(STARTIGNORE)
(Hint):
(Subject): HTTP
(Difficulty): Beginner 
(Applicability): General 
(ENDIGNORE)


(Type): multiplechoice
(Category): 29
(Grade style): 0
(Random answers): 1
(Question): If you visit a website that you just visited a few minutes ago which of the following is MOST likely to be the response code if you have NOT cleared your browser cache?
(A): 200 OK
(B): 304 Not Modified
(C): 404 Not Found
(D): 500 Internal Server Error
(Correct): B
(Points): 1
(CF): Correct!  304 Not Modified is most likely because the content being retrieved is not likely to have changed in the past few minutes.  200 OK would not be expected since your cache had not been cleared, and 404 Not Found and 500 Internal Server Error are not likely since you had just visited the site a few minutes ago.
(WF): 304 Not Modified is most likely because the content being retrieved is not likely to have changed in the past few minutes.  200 OK would not be expected since your cache had not been cleared, and 404 Not Found and 500 Internal Server Error are not likely since you had just visited the site a few minutes ago.
(STARTIGNORE)
(Hint):
(Subject): HTTP
(Difficulty): Beginner 
(Applicability): General 
(ENDIGNORE)


(Type): multiplechoice
(Category): 29
(Grade style): 0
(Random answers): 0
(Question): True or False: GET is a read-only method?
(A): True
(B): False
(Correct): A
(Points): 1
(CF): Correct! Typically GET is used to request a copy of some data, and other methods like POST are used to set data.
(WF): Typically GET is used to request a copy of some data, and other methods like POST are used to set data.
(STARTIGNORE)
(Hint):
(Subject): HTTP
(Difficulty): Beginner 
(Applicability): General 
(ENDIGNORE)


(Type): multiplechoice
(Category): 29
(Grade style): 2
(Random answers): 1
(Question): Which of the following are supported HTTP request header fields?
(A): Cache-Control
(B): User-Agent
(C): Server
(D): Time
(E): Cookie
(Correct): A,B,E
(Points): 1
(CF): 
(WF): 
(STARTIGNORE)
(Hint):
(Subject): HTTP
(Difficulty): Beginner 
(Applicability): General 
(ENDIGNORE)


(Type): multiplechoice
(Category): 29
(Grade style): 2
(Random answers): 1
(Question): Which of the following are supported HTTP response header fields?
(A): Last-Modified
(B): Content-Language
(C): Content
(D): Set-Cookie
(E): Content-Type
(F): Origin
(Correct): A,B,D,E
(Points): 1
(CF): 
(WF): 
(STARTIGNORE)
(Hint):
(Subject): HTTP
(Difficulty): Beginner 
(Applicability): General 
(ENDIGNORE)


(Type): multiplechoice
(Category): 29
(Grade style): 0
(Random answers): 1
(Question): Which of the following is NOT a type of cookie?
(A): Web Session
(B): First Party
(C): Third Party
(D): Second Party
(Correct): D
(Points): 1
(CF): 
(WF): 
(STARTIGNORE)
(Hint):
(Subject): HTTP
(Difficulty): Beginner 
(Applicability): General 
(ENDIGNORE)
