(STARTIGNORE)

Note: Any text within the (STARTIGNORE) and (ENDIGNORE) tags will be ignored when importing this question template

INSTRUCTIONS
Fill in the blank question templates below then upload and check them under the "Tests -> Question bank -> Import questions" section.
Maximum of 50 questions can be uploaded at once

Your categories numbers are listed below for your reference to add to the (Category): line
For example, the line below will use the 'Generic' category

(Category):

Your categories:
0 Generic
1 Java
2 Axure
3 JIRA
4 OOP
5 SDLC
6 Communication
7 Unix Scripting
8 HTML
9 CSS
10 JavaScript
11 Angular JS
12 Design Patterns
13 Git
14 Java Design Pattern
15 Business Analysis
16 Version Control
17 Presentations

(Grade style): Only applicable for 'multiplechoice' (with more then 1 correct answer) style questions
0: Give full score only when all correct responses are provided
1: Give partial score per correct response and deduct partial score for incorrect responses
2: Give partial score per correct response but do not deduct partial score for incorrect responses

(Submitter): This is the name of the person creating and submitting the questions.

(Course Site): This is the site the course was taken on. The options for this are Lynda, and Udemy.

(Course Name): This is the exact name of the course as it is written on the site.

(Course URL): The URL of the course.

(Discipline): This is what discipline the training is classified under, Professional or Technical.

(Hint): This is an optional field that can be used if you wish to provide a hint to the user about the question.

(Subject): This is similar to category but can be used to specify more specific detail on the category.

(Difficulty): This is a range of difficulty of the question, Beginner, Intermediate, and Advanced.

(Applicability): This is if the question is about the course or can be applied to the category in general. Course or General are accepted choices for this section.


(ENDIGNORE)

(STARTIGNORE)
Quiz Details
(Submitter): Bryan Basham
(Course Site): www.udemy.com
(Course Name): Java Swing (GUI) Programming: From Beginner to Expert
(Course URL) : https://www.udemy.com/java-swing-complete/learn/?dtcode=MqxESoN1GfPr#/
(Discipline): Technical 
(ENDIGNORE)


(Type): multiplechoice
(Category): 1
(Grade style): 0
(Random answers): 0
(Question): Given a Swing application that is launched from the MyApp class:
<pre>
1. public class MyApp {
2.    public static void main(String[] args) {
3.       // ADD CODE HERE
4.    }
5. }
</pre>

and the application's main window frame class:
<pre>
public class MainFrame extends JFrame {
   public MyFrame() {
      // initialize View (not shown)
      setVisible(true);  // show the main frame
   }
   // more code here
}
</pre>

Which code snippet on line 3 of <code>MyApp</code> is the recommended way of
launching the application's main frame?
(A): <code>new MainFrame();</code>
(B): <code>EventQueue.launchApp(new MainFrame());</code>
(C): <code>SwingUtilities.invokeLater(new MainFrame());</code>
(D): <code>EventQueue.invokeOnThread(() -> { new MainFrame(); });</code>
(E): <code>SwingUtilities.invokeLater(() -> { new MainFrame(); });</code>
(Correct): E
(Points): 1
(CF): The Swing course recommends that the main frame is constructed in a separate thread from AWT using the <code>SwingUtilities.invokeLater</code> method.
(WF): The Swing course recommends that the main frame is constructed in a separate thread from AWT using the <code>SwingUtilities.invokeLater</code> method.
(STARTIGNORE)
(Subject): Java Swing (GUI) Programming
(Difficulty): Intermediate
(Applicability): Course
(Hint): 
(ENDIGNORE)


(Type): multiplechoice
(Category): 1
(Grade style): 1
(Random answers): 0
(Question): Which four types of components may be added to a Swing menu?  (Choose four)
(A): checkbox
(B): sub menu
(C): radio button
(D): drop-down list
(E): labeled menu item
(F): labeled separator
(Correct): A,B,C,E
(Points): 1
(CF): A menu may contain labeled menu items, sub menus, checkbox and radio button items; separators cannot be labeled.
(WF): A menu may contain labeled menu items, sub menus, checkbox and radio button items; separators cannot be labeled.
(STARTIGNORE)
(Subject): Java Swing (GUI) Programming
(Difficulty): Intermediate
(Applicability): Course
(Hint): 
(ENDIGNORE)


(Type): multiplechoice
(Category): 1
(Grade style): 0
(Random answers): 0
(Question): In the Model-View-Controller architecture pattern, which interaction is NOT allowed?
(A): View talks to Model
(B): Model talks to View
(C): View talks to Controller
(D): Controller talks to View
(E): Controller talks to Model
(Correct): B
(Points): 1
(CF): The Model components must never talk directly to View components.
(CF): The Model components must never talk directly to View components.
(STARTIGNORE)
(Subject): Java Swing (GUI) Programming
(Difficulty): Beginner
(Applicability): General
(Hint): 
(ENDIGNORE)


(Type): multiplechoice
(Category): 1
(Grade style): 1
(Random answers): 0
(Question): Which three are Swing UI components?  (Choose three)
(A): Font
(B): JGrid
(C): JList
(D): JTree
(E): JTable
(F): ComboBox
(G): JDropDownList
(Correct): C,D,E
(Points): 1
(CF): All Swing UI components begin with the letter J, but JGrid and JDropDownList are not a valid component.
(WF): All Swing UI components begin with the letter J, but JGrid and JDropDownList are not a valid component.
(STARTIGNORE)
(Subject): Java Swing (GUI) Programming
(Difficulty): Beginner
(Applicability): Course
(Hint): 
(ENDIGNORE)


(Type): multiplechoice
(Category): 1
(Grade style): 1
(Random answers): 0
(Question): Which three types of dialogs are built into Swing?  (Choose three)
(A): login
(B): message
(C): text input
(D): error trace
(E): confirmation
(Correct): B,C,E
(Points): 1
(CF): The <code>JOptionPane</code> class provides methods to display four types of dialogs: confirmation, input, message and a general options dialog.
(WF): The <code>JOptionPane</code> class provides methods to display four types of dialogs: confirmation, input, message and a general options dialog.
(STARTIGNORE)
(Subject): Java Swing (GUI) Programming
(Difficulty): Beginner
(Applicability): Course
(Hint): 
(ENDIGNORE)


(Type): multiplechoice
(Category): 1
(Grade style): 0
(Random answers): 0
(Question): Which technique promoted perceived UI responsiveness?
(A): Create a new <code>Thread</code> for the expensive operation and start it from the event listener.
(B): Create a new <code>SwingWorker</code> for the expensive operation and start it from the event listener.
(C): Create a new <code>Runnable</code> for the expensive operation and start it from the event listener using the <code>SwingUtilities.invokeLater</code> method.
(D): Create a new <code>SwingWorker</code> for the expensive operation and start it from the event listener using the <code>SwingUtilities.invokeLater</code> method.
(Correct): C
(Points): 1
(CF): Expensive operations should not be executed in the AWT thread; so use a <code>Runnable</code> object and invoke it later.
(WF): Expensive operations should not be executed in the AWT thread; so use a <code>Runnable</code> object and invoke it later.
(STARTIGNORE)
(Subject): Java Swing (GUI) Programming
(Difficulty): Advanced
(Applicability): Course
(Hint): 
(ENDIGNORE)


(Type): multiplechoice
(Category): 1
(Grade style): 0
(Random answers): 0
(Question): Which code snippet creates an accelerator for the <code>exitItem</code> menu item?
(A): <code>exitItem.setMnemonic(KeyEvent.VK_X);</code>
(B): <code>exitItem.setMnemonic(KeyEvent.VK_X, InputEvent.CTRL_DOWN_MASK);</code>
(C): <code>exitItem.setKeyStroke(KeyStroke.getKeyStroke(KeyEvent.VK_X, InputEvent.CTRL_DOWN_MASK));</code>
(D): <code>exitItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_X, InputEvent.CTRL_DOWN_MASK));</code>
(Correct): D
(Points): 1
(CF): An accelerator is a key stroke; not a mnemonic.
(WF): An accelerator is a key stroke; not a mnemonic.
(STARTIGNORE)
(Subject): Java Swing (GUI) Programming
(Difficulty): Intermediate
(Applicability): Course
(Hint): 
(ENDIGNORE)
