(STARTIGNORE)

Note: Any text within the (STARTIGNORE) and (ENDIGNORE) tags will be ignored when importing this question template

INSTRUCTIONS
Fill in the blank question templates below then upload and check them under the "Tests -> Question bank -> Import questions" section.
Maximum of 50 questions can be uploaded at once

Your categories numbers are listed below for your reference to add to the (Category): line
For example, the line below will use the 'Generic' category

(Category):

Your categories:
0 Generic
1 Java
2 Axure
3 JIRA
4 OOP
5 SDLC
6 Communication
7 Unix Scripting
8 HTML
9 CSS
10 JavaScript
11 Angular JS
12 Design Patterns
13 Git
14 Java Design Pattern
15 Business Analysis
16 Version Control
17 Presentations

(Grade style): Only applicable for 'multiplechoice' (with more then 1 correct answer) style questions
0: Give full score only when all correct responses are provided
1: Give partial score per correct response and deduct partial score for incorrect responses
2: Give partial score per correct response but do not deduct partial score for incorrect responses

(Submitter): This is the name of the person creating and submitting the questions.

(Course Site): This is the site the course was taken on. The options for this are Lynda, and Udemy.

(Course Name): This is the exact name of the course as it is written on the site.

(Course URL): The URL of the course.

(Discipline): This is what discipline the training is classified under, Professional or Technical.

(Hint): This is an optional field that can be used if you wish to provide a hint to the user about the question.

(Subject): This is similar to category but can be used to specify more specific detail on the category.

(Difficulty): This is a range of difficulty of the question, Beginner, Intermediate, and Advanced.

(Applicability): This is if the question is about the course or can be applied to the category in general. Course or General are accepted choices for this section.


(ENDIGNORE)

(STARTIGNORE)
Quiz Details
(Submitter): Bryan Basham
(Course Site): www.lynda.com
(Course Name): Foundations of Programming: Design Patterns
(Course URL) : http://www.lynda.com/Developer-Programming-Foundations-tutorials/Foundations-Programming-Design-Patterns/135365-2.html
(Discipline): Technical 
(ENDIGNORE)


(STARTIGNORE)
(Subject): Design Principle questions
(ENDIGNORE)


(Type): multiplechoice
(Category): 12
(Question): Which statement is true about the principle "favor composition over inheritance"?
(A): Composition supports flexible class hierarchies; where as inheritance prevents them
(B): Composition supports run-time reuse; where as inheritance only supports compile-time reuse
(C): Composition supports information hiding; where as inheritance restricts information hiding
(D): Composition supports single responsibility; where as inheritance discourages single responsibility
(Correct): B
(CF): The principle "favor composition over inheritance" supports flexible, run-time composition of behaviors to promote reuse;
 where as inheritance restricts flexibility and reuse is only available through compile-time linkage.
(WF): The principle "favor composition over inheritance" supports flexible, run-time composition of behaviors to promote reuse;
 where as inheritance restricts flexibility and reuse is only available through compile-time linkage.
(STARTIGNORE)
(Subject): Design Principle
(Difficulty): Intermediate
(Applicability): General
(ENDIGNORE)


(Type): multiplechoice
(Category): 12
(Question): You need to write a method that performs a database query and returns a collection of [code]Employee[/code] records.

Which method signature adheres to the "Code to an interface" principle?
(A): [code]public Employee[] searchEmployees(HashMap<String,Object> filters)[/code]
(B): [code]public List<Employee> searchEmployees(Map<String,Object> filters)[/code]
(C): [code]public List<Employee> searchEmployees(HashMap<String,Object> filters)[/code]
(D): [code]public ArrayList<Employee> searchEmployees(HashMap<String,Object> filters)[/code]
(Correct): B
(CF): Only Option B uses all interfaces (List and Map); the other options use some concrete classes (ArrayList and HashMap).
(WF): Only Option B uses all interfaces (List and Map); the other options use some concrete classes (ArrayList and HashMap).
(STARTIGNORE)
(Subject): Design Principle
(Difficulty): Beginner
(Applicability): General
(ENDIGNORE)


(Type): multiplechoice
(Category): 12
(Question): Which statement describes how to achieve a loosely coupled, object-oriented architecture in Java?
(A): Loose coupling is achieved by one class implementing an interface
(B): Loose coupling is achieved by one class inheriting from an abstract class
(C): Loose coupling is achieved when one class depends upon an abstract class or interface
(D): Loose coupling is achieved when two classes inherit from a common abstract class or interface
(Correct): C
(CF): Inheritance forms a tight form of coupling; only option C presents a form of coupling that can be considered "loose".
(WF): Inheritance forms a tight form of coupling; only option C presents a form of coupling that can be considered "loose".
(STARTIGNORE)
(Subject): Design Principle
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 12
(Question): Which three principles result in an increase in cohesion?  (Choose three)
(A): Don't repeat yourself
(B): Open/closed principle
(C): Dependency injection principle
(D): Interface segregation principle
(E): Single responsibility principle
(F): Liskov Substitutibility principle
(Correct): B,D,E
(CF): When you apply any of OCP, ISP and SRP, your design will naturally lead to higher cohesion.
  These other principles can take advantage of cohesion but do not (per se) lead to higher cohesion.
(WF): When you apply any of OCP, ISP and SRP, your design will naturally lead to higher cohesion.
  These other principles can take advantage of cohesion but do not (per se) lead to higher cohesion.
(STARTIGNORE)
(Subject): Design Principle
(Difficulty): Advanced
(Applicability): General
(ENDIGNORE)


(STARTIGNORE)
(Subject): General Pattern questions
(ENDIGNORE)


(Type): multiplechoice
(Category): 12
(Question): Define [i]design pattern[/i].
(A): Design patterns are methods of organization for code
(B): Design patterns are well-tested solutions to common problems in web design
(C): Design patterns are a collection of templates used in designing applications
(D): Design patterns are accepted practices for designing applications and websites
(E): Design patterns are well-tested solutions to common problems in software development
(Correct): E
(WF): A design pattern is a set of rules when coding that help shape your code into a more efficient and secure state for the given task.
(STARTIGNORE)
(Subject): Design Patterns
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 12
(Question): Which two statements are true about design patterns?  (Choose two)
(A): Design patterns provide recipes for implementation
(B): Design patterns frequently have negative consequences
(C): Design patterns simplify communication among developers
(D): Design patterns introduce complexity that hinders reusability
(E): Design patterns are only applicable to object-oriented languages
(Correct): B,C
(CF): Design patterns create a language to simplify dev communication but frequently introduce negative consequences such as design complexity.
(WF): Design patterns create a language to simplify dev communication but frequently introduce negative consequences such as design complexity.
(STARTIGNORE)
(Subject): Design Patterns
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 12
(Question): What is the downside of applying design patterns?
(A): Patterns can make a design confusing
(B): Patterns can make a design inflexible
(C): Patterns constrain the choice of programming language
(D): Patterns are [b]not[/b] applicable to user interface development
(Correct): A
(CF): Patterns always provide greater flexibility but at the cost of greater complexity which can result in confusion about the design.
(WF): Patterns always provide greater flexibility but at the cost of greater complexity which can result in confusion about the design.
(STARTIGNORE)
(Subject): Design Patterns
(Difficulty): Intermediate
(Applicability): General
(ENDIGNORE)


(STARTIGNORE)
(Subject): Observer Pattern questions
(ENDIGNORE)


(Type): multiplechoice
(Category): 12
(Question): Define the [i]Observer[/i] pattern.
(A): Defines a family of algorithms, encapsulates each one, and makes them interchangeable
(B): Defines an interface for creating an object, but lets subclasses decide which class to instantiate
(C): Attach additional responsibilities to an object dynamically; providing a flexible alternative to subclassing
(D): Provides a way to access the elements of an aggregate object sequentially without exposing its underlying representation
(E): Defines one to many dependencies between objects so that when one object changes state, all of its dependants are notified and updated automatically
(Correct): E
(CF): The formal intent of the Observer Pattern is "Defines one to many dependencies between objects so that when one object changes state, all of its dependants are notified and updated automatically"
(WF): The formal intent of the Observer Pattern is "Defines one to many dependencies between objects so that when one object changes state, all of its dependants are notified and updated automatically"
(STARTIGNORE)
(Subject): Design Patterns
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 12
(Question): You are building a weather service. The weather service needs to send up to the minute weather information to radio stations, TV stations and phone apps. 

Which pattern is appropriate for this use case?
(A): State
(B): Strategy
(C): Observer
(D): Decorator
(E): Singleton
(Correct): C
(CF): The Observer pattern is used to notify components that need to know when some data has changed.
(WF): The Observer pattern is used to notify components that need to know when some data has changed.
(STARTIGNORE)
(Subject): Design Patterns
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 12
(Question): The Observer pattern has built-in support in the Java language.

Which pair of Java types are the abstractions for this pattern?
(A): [code]java.awt.Listener[/code] and [code]java.awt.Event[/code]
(B): [code]java.lang.Dependent[/code] and [code]java.lang.Subject[/code]
(C): [code]java.util.Observer[/code] and [code]java.util.Observable[/code]
(D): [code]java.util.EventListener[/code] and [code]java.util.EventObject[/code]
(Correct): C
(CF): Java includes a core Observer pattern classes in: java.util.Observer and java.util.Observable.
(WF): Java includes a core Observer pattern classes in: java.util.Observer and java.util.Observable.
(STARTIGNORE)
(Subject): Design Patterns
(Difficulty): Intermediate
(Applicability): General
(ENDIGNORE)


(STARTIGNORE)
(Subject): Strategy Pattern questions
(ENDIGNORE)


(Type): multiplechoice
(Category): 12
(Question): Define the [i]Strategy[/i] pattern.
(A): Defines a family of algorithms, encapsulates each one, and makes them interchangeable
(B): Defines an interface for creating an object, but lets subclasses decide which class to instantiate
(C): Attach additional responsibilities to an object dynamically; providing a flexible alternative to subclassing
(D): Provides a way to access the elements of an aggregate object sequentially without exposing its underlying representation
(E): Defines one to many dependencies between objects so that when one object changes state, all of its dependants are notified and updated automatically
(Correct): A
(CF): The formal intent of the Strategy Pattern is "Defines a family of algorithms, encapsulates each one, and makes them interchangeable"
(WF): The formal intent of the Strategy Pattern is "Defines a family of algorithms, encapsulates each one, and makes them interchangeable"
(STARTIGNORE)
(Subject): Design Patterns
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 12
(Question): You are building a file import parser.  Each entity is a row in a text file; each entity attribute is a column in the row.
But the Java object's attributes include data types other than String values, such as integers, dates, and references to other entities.

Which pattern would you use to parse the text columns into the appropriate data type?
(A): Builder
(B): Strategy
(C): Iterator
(D): Composite
(E): Decorator
(Correct): B
(CF): The Strategy pattern is the perfect fit for the data conversion behavior of the import process.
(WF): The Strategy pattern is the perfect fit for the data conversion behavior of the import process.
(STARTIGNORE)
(Subject): Design Patterns
(Difficulty): Intermediate
(Applicability): General
(ENDIGNORE)


(Type): multiplechoice
(Category): 12
(Question): Which four design principles are achieved using the Strategy pattern?  (Choose four)
(A): Code to an interface
(B): Open/closed principle (OCP)
(C): Don't repeat yourself (DRY)
(D): Keep it simple, silly (KISS)
(E): Dependency injection principle (DIP)
(F): Single responsibility principle (SRP)
(Correct): A,B,E,F
(CF): The Strategy is a paragon of various design principles, but DRY and KISS aren't among them.
(CF): The Strategy is a paragon of various design principles, but DRY and KISS aren't among them.
(STARTIGNORE)
(Subject): Design Patterns
(Difficulty): Beginner/Intermediate/Advanced
(Applicability): Course/General
(ENDIGNORE)


(STARTIGNORE)
(Subject): Decorator Pattern questions
(ENDIGNORE)


(Type): multiplechoice
(Category): 12
(Question): Define the [i]Decorator[/i] pattern.
(A): Compose objects into tree structures to represent whole-part hierarchies
(B): Provide a surrogate or placeholder for another object to control access to it
(C): Represent an operation to be performed on the elements of an object structure
(D): Attach additional responsibilities to an object dynamically; providing a flexible alternative to subclassing
(E): Separate the construction of a complex object from its representation so that the same construction process can create different representations
(Correct): D
(CF): The formal intent of the Decorator pattern is "Attach additional responsibilities to an object dynamically; providing a flexible alternative to subclassing"
(WF): The formal intent of the Decorator pattern is "Attach additional responsibilities to an object dynamically; providing a flexible alternative to subclassing"
(STARTIGNORE)
(Subject): Design Patterns
(Difficulty): Beginner
(Applicability): General
(ENDIGNORE)


(Type): multiplechoice
(Category): 12
(Question): You are building a point-of-sale app for the local, specialty coffee shop. There are basic products, such as coffee, latte, mocha, but there are variations that affect the total price of the purchase.  For example, a customer can have the drink hot or iced, the customer can request additional shots, the customer can request additional flavors, and so on.  Your first attempt to code this was to create a class hierarchy that encapsulates each combination but this design became unmanageable.

Which design pattern could you use to reduce the class hierarchy chaos and allow any possible combination?
(A): State
(B): Builder
(C): Composite
(D): Decorator
(E): Factory Method
(Correct): D
(CF): The Decorator pattern promotes a flattened class hierarchy and run-time combinations of objects.
(WF): The Decorator pattern promotes a flattened class hierarchy and run-time combinations of objects.
(STARTIGNORE)
(Subject): Design Patterns
(Difficulty): Intermediate
(Applicability): General
(ENDIGNORE)


(STARTIGNORE)
(Subject): State Pattern questions
(ENDIGNORE)


(Type): multiplechoice
(Category): 12
(Question): Define the [i]State[/i] pattern.
(A): Define an object that encapsulates how a set of objects interact
(B): Provide a unified interface to a set of interfaces in a subsystem
(C): Allow an object to alter its behavior when its internal state changes
(D): Attach additional responsibilities to an object dynamically; providing a flexible alternative to subclassing
(Correct): C
(CF): The formal intent of the State pattern is "Allow an object to alter its behavior when its internal state changes"
(WF): The formal intent of the State pattern is "Allow an object to alter its behavior when its internal state changes"
(STARTIGNORE)
(Subject): Design Patterns
(Difficulty): Beginner
(Applicability): General
(ENDIGNORE)


(Type): multiplechoice
(Category): 12
(Question): You are building a complex UI wizard with several screens and "Next" and "Back" buttons for navigation.  However, each screen requires one or more UI fields to be completed before that screen's Next button is enabled.  There is a very precise set of conditions that guide the navigation.

Which two design patterns should you use to encapsulate the navigation logic and decouple the user interaction with the UI fields to that navigation logic?  (Choose two)
(A): State
(B): Facade
(C): Strategy
(D): Observer
(E): Composite
(Correct): A,D
(CF): The State pattern can be used to encapsulate the navigation logic from the rest of the code and the Observer pattern is used to decouple the fields from this nav logic.
(WF): The State pattern can be used to encapsulate the navigation logic from the rest of the code and the Observer pattern is used to decouple the fields from this nav logic.
(STARTIGNORE)
(Subject): Design Patterns
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(STARTIGNORE)
(Subject): Singleton Pattern questions
(ENDIGNORE)


(Type): multiplechoice
(Category): 12
(Question): Define the [i]Singleton[/i] pattern.
(A): Provide a unified interface to a set of interfaces in a subsystem
(B): Provide a surrogate or placeholder for another object to control access to it
(C): Ensure a class has only one instance, and provide a global point of access to it
(D): Defines an interface for creating an object, but lets subclasses decide which class to instantiate
(Correct): C
(CF): The formal intent of the Singleton pattern is "Ensure a class has only one instance, and provide a global point of access to it"
(WF): The formal intent of the Singleton pattern is "Ensure a class has only one instance, and provide a global point of access to it"
(STARTIGNORE)
(Subject): Design Patterns
(Difficulty): Beginner
(Applicability): General
(ENDIGNORE)


(Type): multiplechoice
(Category): 12
(Question): The Singleton design pattern can be implemented in a variety of different ways.

Which three are common gotchas when writing a Java Singleton? (Choose three)
(A): Threaded access to the Singleton could result in deadlock
(B): A Singleton component used by multiple threads could result in data corruption
(C): Threaded access to the Singleton could result in two copies of the Singleton object
(D): A Singleton component distributed across several servers can get out-of-sync with each other
(E): Deserialization of a Singleton on input stream can result in two copies of the Singleton object
(Correct): B,C,D
(CF): There will be multiple instances of a Singleton across multiple servers each with different data.  An unsynchronized accessor method can result in two Singleton objects created if two threads attempt to access concurrently.  Unsyncrhonized instance methods can result in data corruption from multi-threaded access.
(WF): There will be multiple instances of a Singleton across multiple servers each with different data.  An unsynchronized accessor method can result in two Singleton objects created if two threads attempt to access concurrently.  Unsyncrhonized instance methods can result in data corruption from multi-threaded access.
(STARTIGNORE)
(Subject): Design Patterns
(Difficulty): Intermediate/Advanced
(Applicability): General
(ENDIGNORE)


(STARTIGNORE)
(Subject): Iterator Pattern questions
(ENDIGNORE)


(Type): multiplechoice
(Category): 12
(Question): Define the [i]Iterator[/i] pattern.
(A): Define an object that encapsulates how a set of objects interact
(B): Allow an object to alter its behavior when its internal state changes
(C): Compose objects into tree structures to represent whole-part hierarchies
(D): Provides a way to access the elements of an aggregate object sequentially without exposing its underlying representation
(Correct): D
(CF): The formal intent of the Iterator pattern is "Provides a way to access the elements of an aggregate object sequentially without exposing its underlying representation"
(WF): The formal intent of the Iterator pattern is "Provides a way to access the elements of an aggregate object sequentially without exposing its underlying representation"
(STARTIGNORE)
(Subject): Design Patterns
(Difficulty): Beginner
(Applicability): General
(ENDIGNORE)


(Type): multiplechoice
(Category): 12
(Question): You need to build a tree structure that mimics a file system with branch nodes that represent directories and leaf nodes that represent files or aliases.  Your application then needs to walk the tree using several traversal techniques such as depth-first and breadth-first.

Which two design patterns should you use to create this tree structure and then perform traversal to walk the tree?  (Choose two)
(A): State
(B): Iterator
(C): Strategy
(D): Mediator
(E): Composite
(Correct): B,E
(CF): The Composite pattern facilitates the creation of a tree structure and the Iterator pattern can be used to create multiple, different traversal algorithms. 
(WF): The Composite pattern facilitates the creation of a tree structure and the Iterator pattern can be used to create multiple, different traversal algorithms. 
(STARTIGNORE)
(Subject): Design Patterns
(Difficulty): Advanced
(Applicability): General
(ENDIGNORE)


(Type): multiplechoice
(Category): 12
(Question): The Java collections API include the [code]java.util.Iterator[/code] interface.

Which three methods are included in that interface?  (Choose three)
(A): [code]get[/code]
(B): [code]next[/code]
(C): [code]remove[/code]
(D): [code]insert[/code]
(E): [code]hasNext[/code]
(F): [code]previous[/code]
(Correct): B,C,E
(CF): The only methods on the java.util.Iterator interface are: next, hasNext, and remove.
(WF): The only methods on the java.util.Iterator interface are: next, hasNext, and remove.
(STARTIGNORE)
(Subject): Design Patterns
(Difficulty): Beginner/Intermediate/Advanced
(Applicability): Course/General
(ENDIGNORE)


(STARTIGNORE)
(Subject): Factory Method Pattern questions
(ENDIGNORE)


(Type): multiplechoice
(Category): 12
(Question): Define the [i]Factory Method[/i] pattern.
(A): Compose objects into tree structures to represent whole-part hierarchies
(B): Provide a surrogate or placeholder for another object to control access to it
(C): Defines an interface for creating an object, but lets subclasses decide which class to instantiate
(D): Separate the construction of a complex object from its representation so that the same construction process can create different representations
(Correct): C
(CF): The formal intent of the Factory Method pattern is "Defines an interface for creating an object, but lets subclasses decide which class to instantiate"
(WF): The formal intent of the Factory Method pattern is "Defines an interface for creating an object, but lets subclasses decide which class to instantiate"
(STARTIGNORE)
(Subject): Design Patterns
(Difficulty): Beginner
(Applicability): General
(ENDIGNORE)


(Type): multiplechoice
(Category): 12
(Question): Java's [code]java.util.Collection[/code] interface provides a method, called [code]iterator[/code], that returns an object that implements the [code]java.util.Iterator[/code] interface for a specific type of collection.

The [code]iterator[/code] method is an example of which design pattern?
(A): Adapter
(B): Builder
(C): Flyweight
(D): Factory Method
(E): Abstract Factory
(Correct): D
(CF): The iterator() method is an example of the Factory Method pattern because it depends upon concrete subclasses of Collection to create the appropriate iterator object.
(WF): The iterator() method is an example of the Factory Method pattern because it depends upon concrete subclasses of Collection to create the appropriate iterator object.
(STARTIGNORE)
(Subject): Design Patterns
(Difficulty): Intermediate
(Applicability): General
(ENDIGNORE)


(Type): multiplechoice
(Category): 12
(Question): You are building an online training application in which users take quizzes that are then graded. Quizzes are composed of assessment items of differing types with different algorithms for grading; each grading algorithm is encapsulated in a distinct concrete class.  The abstract [code]AssessmentItem[/code] class has a method called [code]makeGrade[/code] which returns an abstract [code]Grade[/code] object.  The [code]makeGrade[/code] method is implemented by specific subclasses.

Which design pattern is being used by this scenario?
(A): Adapter
(B): Builder
(C): Composite
(D): Factory Method
(Correct): D
(CF): The Factory Method "defines an interface for creating an object, but lets subclasses decide which class to instantiate."
(WF): The Factory Method "defines an interface for creating an object, but lets subclasses decide which class to instantiate."
(STARTIGNORE)
(Subject): Design Patterns
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)
