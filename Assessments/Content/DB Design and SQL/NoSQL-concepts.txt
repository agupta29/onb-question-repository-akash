==========
Objective: Given a design scenario, select an appropriate data store type
==========

[[
Your application requires the storage of service configuration that has varying
complex structures with combinations of lists and maps.  Furthermore, as
the application changes so too does the structure of the configuration data.

Which type of data store is suitable for the storage of this configuration data?
]]
1: Graph store
2: Key-value store
*3: Document database
4: Relational database
