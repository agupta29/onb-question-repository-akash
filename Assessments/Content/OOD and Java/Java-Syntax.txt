==========
Objective: Given a scenario select an appropriate type construct: primitives, class, abstract class,
 interface, anonymous inner class, lambda (anonymous function), enum, or nested types .
==========

[[
You are building a desktop application for playing a variety of card games.
You need to represent the rank (2, 3, ... 10, Jack, Queen, King, Ace) of each card.
The set of card ranks is fixed (2 through Ace) and client code should <em>not</em> be able
to create other instances.
<br/><br/>
Which Java type would you use?
]]
*1: An enumeration
2: A concrete class
2: An abstract class
3: An anonymous inner class


[[
You are writing the code for an <code>ActionListener</code> to attach to a UI button.
<br/><br/>
Which three Java types could you use?  (Choose three)
]]
1: An interface
*2: A concrete class
*3: A lambda function
4: An abstract class
*5: An anonymous inner class


==========
Objective: Write code to create any type 
==========

[[
In a medical records application, the <code>Patient</code> needs to record
the martial status of the patient.  The martial status type must be restricted
to four values: single, married, widowed and divorced.
<br/><br/>
Write the Java code to create the <code>MaritalStatus</code> data type.
]]
1: <pre>public class MaritalStatus {
   SINGLE,
   MARRIED,
   WIDOWED,
   DIVORCED
}</pre>
2: <pre>public class MaritalStatus {
   public static final MaritalStatus SINGLE = new MaritalStatus();
   public static final MaritalStatus MARRIED = new MaritalStatus();
   public static final MaritalStatus WIDOWED = new MaritalStatus();
   public static final MaritalStatus DIVORCED = new MaritalStatus();
}</pre>
*3: <pre>public enum MaritalStatus {
   SINGLE,
   MARRIED,
   WIDOWED,
   DIVORCED
}</pre>
4: <pre>public enum MaritalStatus {
   public static final MaritalStatus SINGLE = new MaritalStatus();
   public static final MaritalStatus MARRIED = new MaritalStatus();
   public static final MaritalStatus WIDOWED = new MaritalStatus();
   public static final MaritalStatus DIVORCED = new MaritalStatus();
}</pre>


[[
Which declaration is the minimum amount of code needed to
create a concrete class with a single, no-argument constructor?
]]
*1: <pre>public class MyClass {
}</pre>
2: <pre>public class MyClass extends Object {
}</pre>
3: <pre>public class MyClass {
   public MyClass() {}
}</pre>
4: <pre>public class MyClass extends Object {
   public MyClass() {}
}</pre>

[[THOUGHT: add an item that tests that a no-arg ctor is dropped when any other ctor is added.]]
[[
You have built a simple POJO that is intended to be JavaBean.  Here is the code:

<pre>public final class MyClass implements java.io.Serializable {
   private final String data;
   public MyClass(final String data) { this.data = data; }
   public String getData() { return this.data; }
}</pre>

Which two are reasons why this class does <em>not</em> follow the JavaBean specification?  (Choose two)
]]
*1: There must be a no-arg constructor
2:  The class must also have a <code>setData</code> method
3:  The <code>serialVersionUID</code> constant is required
*4:  The instance variables must <em>not</em> be <code>final</code>
5:  The <code>Serializable</code> interface requires an implementation of the <code>equals</code> method


==========
Objective: Identify proper data literal syntax, including syntax introduced by Project Coin in Java 7.
==========

[[
Which four code snippets are valid integer literals that represents one million?  (Choose three)
]]
*1: <code>1000000</code>
*2: <code>0xF4240</code>
3: <code>1000000.</code>
4: <code>01000000</code>
*5: <code>1_00_00_00</code>
6: <code>1,000,000</code>
*7: <code>0b1111_01000010_01000000</code>
[[THOUGHT: niche concepts; find a better concept.
EG syntax of decimal literals: 0.0d vs 0.0f
]]

[[
Which two Java code snippets correctly declare and initialize a floating
point variable holding the approximate value of pi?  (Choose two)
]]
1:  <code>float pi = 3.141592;</code>
*2: <code>float pi = 3.141592f;</code>
3:  <code>float pi = 3.141592d;</code>
*4: <code>float pi = 3.141_592f;</code>
5:  <code>float pi = 0.3141592e+01;</code>


==========
Objective: Explain the member visibility rules
TBD
==========


==========
Objective: Write an entry point (main) method (or at least the signature)
==========

[[
Which code snippet represents the declaration of a Java program entry point?
]]
1: <code>static int main()</code>
2: <code>private int main(argc, argv)</code>
3: <code>public static int main(char* args)</code>
4: <code>public static void main(char* args)</code>
*5: <code>public static void main(String[] args)</code>

